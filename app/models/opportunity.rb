# == Schema Information
#
# Table name: opportunities
#
#  id              :integer          not null, primary key
#  title           :text
#  image           :string
#  description     :text
#  summary         :text
#  people_needed   :integer
#  start_date      :datetime
#  end_date        :datetime
#  address         :text
#  latitude        :float
#  longitude       :float
#  organization_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  category        :integer          default(0)
#  city            :string
#  state           :string
#  deleted_at      :datetime
#  status          :integer          default(0)
#

class Opportunity < ActiveRecord::Base
  class AlreadyClosed < StandardError; end

  include Filterable
  include Localizable
  include Paranoiable

  validates :title,
            :description,
            :summary,
            :start_date,
            :end_date,
            :address,
            :latitude,
            :longitude,
            :city,
            :state,
            presence: true, if: -> { status == 1 }

  validates :title,       length: { maximum: 50 }
  validates :summary,     length: { maximum: 200 }
  validates :description, length: { maximum: 1000 }

  CATEGORIES = {
      'Community'       => 0,
      'Poor/Homeless'   => 1,
      'Animals'         => 2,
      'Children/Teens'  => 3,
      'Elderly/Sick'    => 4
  }

  STATUSES = {
      'Not published'   => 0,
      'Published'       => 1,
      'Closed'          => 2
  }

  enum_without_methods category: CATEGORIES

  enum_without_methods status: STATUSES

  mount_uploader :image, ImageUploader

  belongs_to  :organization

  has_many    :opportunity_contacts, dependent: :destroy
  has_many    :contacts, through: :opportunity_contacts

  has_many    :user_opportunities, dependent: :destroy
  has_many    :users, through: :user_opportunities

  scope :starts_before,     lambda { |date| where('opportunities.start_date <= ?', Time.parse(date.to_s)) }
  scope :starts_after,      lambda { |date| where('opportunities.start_date >= ?', Time.parse(date.to_s)) }

  scope :ends_before,       lambda { |date| where('opportunities.end_date <= ?', Time.parse(date.to_s)) }
  scope :ends_after,        lambda { |date| where('opportunities.end_date >= ?', Time.parse(date.to_s)) }

  scope :updated_before,    lambda { |date| where('opportunities.updated_at <= ?', Time.parse(date.to_s)) }
  scope :updated_after,     lambda { |date| where('opportunities.updated_at >= ?', Time.parse(date.to_s)) }

  scope :with_category,     lambda { |category_id| where('opportunities.category = ?', category_id) }

  scope :with_status,       lambda { |status_id| where('opportunities.status = ?', status_id) }
  scope :not_published,     lambda { where(status: 0) }
  scope :published,         lambda { where(status: 1) }
  scope :closed,            lambda { where(status: 2) }


  def close!
    raise AlreadyClosed if closed?

    update_attributes!(status: 2)
  end

  def category=(category_id)
    write_attribute(:category, category_id.to_i)
  end

  def not_published?
    status == 'Not published'
  end

  def published?
    status == 'Published'
  end

  def closed?
    status == 'Closed'
  end

  def self.categories
    CATEGORIES
  end

  def status=(status_id)
    write_attribute(:status, status_id.to_i)
  end

  def self.statuses
    STATUSES
  end

  def self.status_name_for(id)
    key_val = STATUSES.select { |k, v| v.to_s == id.to_s }
    return unless key_val.any?

    key_val.keys.first
  end
end
