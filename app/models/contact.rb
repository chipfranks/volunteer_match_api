# == Schema Information
#
# Table name: contacts
#
#  id              :integer          not null, primary key
#  first_name      :string
#  last_name       :string
#  phone           :string
#  email           :string
#  image           :string
#  organization_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  deleted_at      :datetime
#

class Contact < ActiveRecord::Base
  include Paranoiable

  mount_uploader :image, ImageUploader

  belongs_to :organization

  validates :first_name, :last_name, :phone, :email, presence: true

  def name
    [self.first_name, self.last_name].join ' '
  end
end
