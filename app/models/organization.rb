# == Schema Information
#
# Table name: organizations
#
#  id          :integer          not null, primary key
#  name        :text
#  image       :string
#  website_url :string
#  address     :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  deleted_at  :datetime
#

class Organization < ActiveRecord::Base
  include Filterable
  include Paranoiable

  before_save do
    if self.website_url
      self.website_url = "http://#{self.website_url}" unless self.website_url =~ /^https?:\/\//
    end
  end


  validates :name, :address, presence: true

  mount_uploader :image, ImageUploader

  has_many :opportunities,  dependent: :destroy
  has_many :contacts,       dependent: :destroy
  has_many :members,        dependent: :nullify, class_name: 'User'

  scope :with_name, lambda { |query_name| where('lower(organizations.name) like ?', "%#{query_name.to_s.downcase}%") }
end
