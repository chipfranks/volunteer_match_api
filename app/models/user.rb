# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  first_name             :string           default("")
#  last_name              :string           default("")
#  image                  :string
#  gender                 :integer          default(0)
#  facebook_uid           :string
#  organization_id        :integer
#  deleted_at             :datetime
#  twitter_uid            :string
#

class User < ActiveRecord::Base
  class AlreadyJoined < StandardError; end
  class DidNotJoin < StandardError; end

  include Paranoiable

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  enum gender: {
      male:   0,
      female: 1
  }

  mount_uploader :image, ImageUploader

  has_many :user_opportunities, dependent: :destroy
  has_many :opportunities, through: :user_opportunities

  belongs_to :organization

  before_create :ensure_has_image

  def joined?(opportunity)
    opportunities.pluck(:id).include?(opportunity.id)
  end

  def name
    [self.first_name, self.last_name].join ' '
  end

  def self.default_image
    Rails.root.join('app/assets/images/user_default_image.png')
  end

  private

  def ensure_has_image
    unless self.image.file
      self.image = self.class.default_image.open
    end
  end
end
