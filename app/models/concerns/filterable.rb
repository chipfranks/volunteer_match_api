module Filterable
  extend ActiveSupport::Concern

  included do
    def self.filter_with(params)
      params.inject(all) do |result, (key, value)|
        result = value.present? ? result.public_send(key, value) : result.public_send(key)
        result
      end
    end
  end
end