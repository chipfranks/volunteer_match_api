module Paranoiable
  extend ActiveSupport::Concern

  included do
    acts_as_paranoid

    alias_method :deleted, :deleted?

    scope :deletion_scope, -> (kind) { public_send(kind) if kind.to_s.in? %w( with_deleted without_deleted only_deleted ) }
  end
end
