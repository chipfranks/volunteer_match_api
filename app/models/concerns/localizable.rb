module Localizable
  extend ActiveSupport::Concern

  included do
    scope :with_distance_to_less_than,     lambda { |query_string| with_distance_to(query_string, '<=') }
    scope :with_distance_to_greater_than,  lambda { |query_string| with_distance_to(query_string, '>=') }

    private

    scope :with_distance_to, lambda { |query_string, inequality|
      lat, long, dist = query_string.split('|')

      return if lat.nil? || !lat.respond_to?(:to_f) || long.nil? || !long.respond_to?(:to_f) || dist.nil? || !dist.respond_to?(:to_f)

      # to float
      lat, long, dist = [lat, long, dist].map(&:to_f)

      where("(point(latitude, longitude) <@> point(#{lat}, #{long}))::numeric #{inequality} #{dist}")
    }
  end
end