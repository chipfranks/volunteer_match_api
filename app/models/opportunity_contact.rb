# == Schema Information
#
# Table name: opportunity_contacts
#
#  id             :integer          not null, primary key
#  contact_id     :integer
#  opportunity_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  deleted_at     :datetime
#

class OpportunityContact < ActiveRecord::Base
  include Paranoiable

  belongs_to :contact
  belongs_to :opportunity
end
