# == Schema Information
#
# Table name: user_opportunities
#
#  id             :integer          not null, primary key
#  user_id        :integer
#  opportunity_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  deleted_at     :datetime
#

class UserOpportunity < ActiveRecord::Base
  include Paranoiable

  belongs_to :user
  belongs_to :opportunity
end
