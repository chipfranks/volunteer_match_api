module ApplicationHelper

  def retina_image_tag(source, options = {})
    original = source.gsub(%r{\.\w+$}, '-@1x\0')
    retina = source.gsub(%r{\.\w+$}, '-@2x\0')

    image_tag(original, options.merge(srcset: "#{path_to_image(retina)} 2x"))
  end
end
