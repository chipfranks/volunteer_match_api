# == Schema Information
#
# Table name: opportunities
#
#  id              :integer          not null, primary key
#  title           :text
#  image           :string
#  description     :text
#  summary         :text
#  people_needed   :integer
#  start_date      :datetime
#  end_date        :datetime
#  address         :text
#  latitude        :float
#  longitude       :float
#  organization_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  category        :integer          default(0)
#  city            :string
#  state           :string
#  deleted_at      :datetime
#  status          :integer          default(0)
#

class OpportunitiesController < PanelController
  before_action :set_opportunity, only: %i(destroy close)

  def index
    opportunities = organization.opportunities.filter_with(opportunity_filters)

    if params[:sort_by] && params[:sort_by].to_sym.in?(allowed_sorting_params)
      opportunities = opportunities.order("#{params[:sort_by]} DESC")
    else
      opportunities = opportunities.order('start_date DESC')
    end

    opportunities = opportunities.limit(25)

    @serialized_opportunities = serialized(opportunities, include: %w(users))
    @serialized_categories    = Opportunity.categories
    @serialized_statuses      = Opportunity.statuses
  end

  def new
    @opportunity = organization.opportunities.build
    serialize_models
  end

  def create
    @opportunity = organization.opportunities.build(opportunity_params)

    unless @opportunity.contacts.any?
      contact = organization.contacts.build(contact_params)
      @opportunity.contacts << contact if contact.valid?
    end

    if @opportunity.valid?
      @profanity_filter ||= ProfanityFilter.new

      @opportunity.title        = @profanity_filter.clean(@opportunity.title)
      @opportunity.summary      = @profanity_filter.clean(@opportunity.summary)
      @opportunity.description  = @profanity_filter.clean(@opportunity.description)

      @opportunity.save(validate: false)

      redirect_to action: :show, id: @opportunity.id and return
    end

    serialize_models

    render :new
  end

  def show
    @opportunity = Opportunity.find(params[:id])

    authorize @opportunity

    @serialized_categories  = Opportunity.categories
    @serialized_opportunity = serialized(@opportunity, include: %w(contacts organization users))
  end

  def edit
    @opportunity = Opportunity.find(params[:id])

    authorize @opportunity

    serialize_models
  end

  def update
    @opportunity = Opportunity.find(params[:id])

    authorize @opportunity

    @opportunity.assign_attributes(opportunity_params)

    unless opportunity_params[:contact_ids].present? && opportunity_params[:contact_ids].first.present?
      contact = organization.contacts.build(contact_params)
      @opportunity.contacts << contact if contact.valid?
    end

    if @opportunity.valid?
      @profanity_filter ||= ProfanityFilter.new

      @opportunity.title        = @profanity_filter.clean(@opportunity.title)
      @opportunity.summary      = @profanity_filter.clean(@opportunity.summary)
      @opportunity.description  = @profanity_filter.clean(@opportunity.description)

      @opportunity.save(validate: false)

      redirect_to action: :show, id: @opportunity.id and return
    end

    serialize_models

    render :edit
  end

  def destroy
    @opportunity.destroy

    authorize @opportunity

    redirect_to action: :index
  end

  def close
    @opportunity.close!

    authorize @opportunity

    redirect_to action: :index
  end

  private

  def organization
    current_user.organization
  end

  def set_opportunity
    @opportunity ||= Opportunity.find(params[:id])
  end

  def opportunity_attributes
    %i(
      title
      image
      description
      summary
      people_needed
      start_date
      end_date
      address
      latitude
      longitude
      category
      city
      state
      status
    ) << { contact_ids: [] }
  end

  def contact_attributes
    %i(image first_name last_name phone email)
  end

  def opportunity_params
    params.require(:opportunity).permit opportunity_attributes
  end

  def contact_params
    params.require(:contact).permit contact_attributes
  end

  def allowed_sorting_params
    %i(start_date created_at)
  end

  def opportunity_filters
    params.permit %i( with_status )
  end

  def serialize_models
    @serialized_categories  = Opportunity.categories
    @serialized_opportunity = serialized(@opportunity, include: %w(contacts organization))
    @serialized_contacts    = serialized(@opportunity.organization.contacts)
  end
end
