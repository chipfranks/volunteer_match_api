class HomeController < ApplicationController

  def index
  end

  def volunteers
  end

  def organizations
  end

  def about
  end

  def privacy_policy
  end

  def terms_of_use
  end
end