module Api
  class OpportunitiesController < Api::CrudController
    skip_before_filter :doorkeeper_authorize!, only: %i(index show)

    def update
      authorize model

      contact_ids = normalize_array(params[:contact_ids])

      model.assign_attributes(allowed_params)
      model.contact_ids = contact_ids

      if model.valid?
        @profanity_filter ||= ProfanityFilter.new

        model.title        = @profanity_filter.clean(model.title)
        model.summary      = @profanity_filter.clean(model.summary)
        model.description  = @profanity_filter.clean(model.description)

        model.save(validate: false)

        render json: model
        true
      else
        invalid
        false
      end
    end

    def join
      authorize model

      UserOpportunitiesManager.new(current_user, model).join!

      render json: model
    end

    def leave
      authorize model

      UserOpportunitiesManager.new(current_user, model).leave!

      render json: model
    end

    def close
      authorize model

      model.close!

      render json: model
    end

    def collection
      Opportunity.deletion_scope(params[:deletion_scope])
    end

    def model_includes
      %i(organization contacts users)
    end

    def model_filters
      %i(
          starts_after
          starts_before
          ends_after
          ends_before
          updated_after
          updated_before
          with_category
          with_distance_to_less_than
          with_distance_to_greater_than
          with_status
        )
    end

    def model_params
      params.permit %i(title image description summary people_needed start_date end_date address latitude longitude category city state)
    end
  end
end