module Api
  class CrudController < Api::ApplicationController
    # :nocov:
    def collection
      raise 'Not implemented'
    end

    def model_params
      raise 'Not implemented'
    end

    def model_defaults
      {}
    end

    def model_filters
      []
    end

    def model_includes
      []
    end

    def collection_paginates?
      true
    end
    # :nocov:

    def allowed_params
      model_defaults.merge model_params
    end

    def allowed_filters
      params.permit(model_filters)
    end

    def allowed_includes
      normalize_array(params[:includes]).map(&:to_sym) & model_includes
    end

    def allowed_orders
      normalize_array(params[:order]).map { |param| param.gsub(':', ' ').downcase }
    end

    # Actions

    def index
      # authorization here we do using policy_scope in descendant controller
      collection = self.collection

      if collection.respond_to?(:filter_with) && allowed_filters.any?
        collection = self.collection.filter_with(allowed_filters)
      end

      if collection.respond_to?(:order) && allowed_orders.any?
        collection = self.collection.order(allowed_orders)
      end

      collection = paginated(collection)

      render json: collection, include: allowed_includes
    end

    def show
      authorize model

      render json: model, include: allowed_includes
    end

    def create
      authorize model

      if model.save
        render json: model
        true
      else
        invalid
        false
      end

    rescue ActiveRecord::RecordInvalid
      invalid
      false
    end

    def update
      authorize model

      if model.update(allowed_params)
        render json: model
        true
      else
        invalid
        false
      end
    end

    def destroy
      authorize model

      model.destroy
      render json: model
    end

    protected
    def model
      @model ||= ((params[:id] && collection.find(params[:id])) || collection.new(allowed_params))
    end

    def invalid(invalid_model=nil)
      invalid_model ||= self.model
      error "invalid_#{invalid_model.class.name.underscore}", invalid_model.errors.full_messages.join("\n"), validation: invalid_model.errors
    end

    def paginated(collection)
      if collection_paginates? && collection.respond_to?(:page)
        page = params[:page].present? ? params[:page][:number] : 1
        per_page = params[:page].present? ? params[:page][:size] : 25

        collection = collection.page(page).per(per_page)
      end

      collection
    end
  end
end