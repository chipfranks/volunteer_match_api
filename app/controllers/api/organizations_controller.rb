module Api
  class OrganizationsController < Api::CrudController
    skip_before_filter :doorkeeper_authorize!, only: %i(index show)

    def collection
      Organization.deletion_scope(params[:deletion_scope])
    end

    def model_includes
      %i(opportunities contacts)
    end

    def model_filters
      %i(with_name)
    end

    def model_params
      params.permit %i(name image website_url address)
    end
  end
end