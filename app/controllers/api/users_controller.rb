module Api
  class UsersController < Api::CrudController
    skip_before_filter :doorkeeper_authorize!, only: %i(image)

    def image
      file = model.image.file

      if file.nil?
        error 'not found', 'image not found', {}, 404 and return
      end

      send_file file.path, type: file.content_type, disposition: 'inline'
    end

    def collection
      User.deletion_scope(params[:deletion_scope])
    end
  end
end