module Api
  class ApplicationController < ::ApplicationController
    before_filter :doorkeeper_authorize!

    layout false

    respond_to :json

    def valid_doorkeeper_token?
      doorkeeper_token && doorkeeper_token.acceptable?(@_doorkeeper_scopes) && current_user.present?
    end

    def doorkeeper_invalid_token_response?
      !doorkeeper_token || !doorkeeper_token.accessible? || current_user.nil?
    end

    rescue_from StandardError, with: :handle_api_error

    def handle_api_error(exception)
      Rollbar.error(exception)
      Rails.logger.error exception.message + "\n" + exception.backtrace.join("\n")

      hsh = { exception_name: exception.class.name }
      hsh[:backtrace] = exception.backtrace unless Rails.env.production?

      if exception.respond_to?(:record) # most likely ActiveRecord error
        error_name = "invalid_#{exception.record.class.name.underscore}"
        hsh[:errors] = error_messages(exception)
      else
        error_name = 'error'
      end

      status = exception.is_a?(ActiveRecord::RecordNotFound) ? 404 : 500
      error(error_name, exception.message, hsh, status)
    end

    def error(message, description=nil, opts_or_status={}, status=500)
      opts, status = (opts_or_status.is_a?(Hash) ? [opts_or_status, status] : [{}, opts_or_status])
      description ||= message

      self.response_body = nil # clear current rendering buffer

      render json: opts.reverse_merge({ error: message.to_s, error_description: description.to_s }), status: status
    end

    def doorkeeper_unauthorized_render_options(error: nil)
      {
          json: {
              error: error.respond_to?(:name) ? error.name : 'unauthorized',
              error_description: error.respond_to?(:description) ? error.description : '',
              status: 401
          }
      }
    end

    def doorkeeper_forbidden_render_options(error: nil)
      {
          json: {
              error: error.respond_to?(:name) ? error.name : 'forbidden',
              error_description: error.respond_to?(:description) ? error.description : '',
              status: 403
          }
      }
    end

    def current_user
      @current_user ||= ::User.find_by(id: doorkeeper_token.try(:resource_owner_id))
    end

    def error_messages(exception)
      errors = exception.record.errors
      errors.map.inject({}) { |err, (attribute, message)| err[attribute] = errors.full_message(attribute, message); err }
    end

    def normalize_array(param)
      param.is_a?(String) ? param.gsub(/\[|\]/, '').split(',') : Array.wrap(param)
    end

    private

    def append_info_to_payload(payload)
      super
      authorization = request.headers['Authorization']
      payload[:access_token] = authorization.sub(/^bearer /i, '') if authorization.present?
      payload
    end
  end
end