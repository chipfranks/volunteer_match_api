class Api::MeController < Api::ApplicationController
  skip_before_filter :doorkeeper_authorize!, only: :create

  def index
    render json: current_user
  end

  def create
    user = ::User.create!(user_params)
    render json: user
  end

  def update
    current_user.update!(user_params)
    render json: current_user
  end

  private

  def user_params
    params.permit %i(email password first_name last_name image)
  end
end