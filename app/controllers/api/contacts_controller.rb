module Api
  class ContactsController < Api::CrudController
    skip_before_filter :doorkeeper_authorize!, only: %i(index show)

    def collection
      Contact.deletion_scope(params[:deletion_scope])
    end

    def model_includes
      %i(organization)
    end
  end
end