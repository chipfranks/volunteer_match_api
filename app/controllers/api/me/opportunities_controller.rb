module Api
  module Me
    class OpportunitiesController < Api::CrudController

      def collection
        current_user.opportunities.deletion_scope(params[:deletion_scope])
      end

      def model_includes
        %i(organization contacts users)
      end

      def model_filters
        %i(
          starts_after
          starts_before
          ends_after
          ends_before
          updated_after
          updated_before
          with_category
          with_distance_to_less_than
          with_distance_to_greater_than
          with_status
        )
      end
    end
  end
end