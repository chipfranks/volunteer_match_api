module Api
  module Organizations
    class ContactsController < Api::CrudController
      skip_before_filter :doorkeeper_authorize!, only: %i(index show)

      def collection
        organization.contacts.deletion_scope(params[:deletion_scope])
      end

      def organization
        @organization ||= Organization.find(params[:organization_id])
      end

      def model_defaults
        { organization_id: organization.id }
      end

      def model_includes
        %i(organization)
      end

      def model_params
        params.permit %i(first_name last_name phone email image)
      end
    end
  end
end