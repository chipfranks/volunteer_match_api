module Api
  module Organizations
    class OpportunitiesController < Api::CrudController
      skip_before_filter :doorkeeper_authorize!, only: %i(index show)

      def create
        authorize model

        contact_ids = normalize_array(params[:contact_ids])
        model.contact_ids = contact_ids

        if model.valid?
          @profanity_filter ||= ProfanityFilter.new

          model.title        = @profanity_filter.clean(model.title)
          model.summary      = @profanity_filter.clean(model.summary)
          model.description  = @profanity_filter.clean(model.description)

          model.save(validate: false)

          render json: model
          true
        else
          invalid
          false
        end

      rescue ActiveRecord::RecordInvalid
        invalid
        false
      end

      def collection
        organization.opportunities.deletion_scope(params[:deletion_scope])
      end

      def organization
        @organization ||= Organization.find(params[:organization_id])
      end

      def model_defaults
        { organization_id: organization.id }
      end

      def model_includes
        %i(organization contacts users)
      end

      def model_filters
        %i(
          starts_after
          starts_before
          ends_after
          ends_before
          updated_after
          updated_before
          with_category
          with_distance_to_less_than
          with_distance_to_greater_than
          with_status
        )
      end

      def model_params
        params.permit %i(title image description summary people_needed start_date end_date address latitude longitude category city state)
      end
    end
  end
end