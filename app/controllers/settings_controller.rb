# == Schema Information
#
# Table name: organizations
#
#  id          :integer          not null, primary key
#  name        :text
#  image       :string
#  website_url :string
#  address     :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  deleted_at  :datetime
#

class SettingsController < PanelController

  def edit
    @serialized_organization = serialized(organization)
  end

  def update
    if organization.update(organization_params)
      redirect_to action: :edit and return
    end

    @serialized_organization = serialized(organization)

    render :edit
  end

  private

  def organization
    current_user.organization
  end

  def organization_attributes
    %i(name image website_url address)
  end

  def organization_params
    params.require(:organization).permit organization_attributes
  end
end
