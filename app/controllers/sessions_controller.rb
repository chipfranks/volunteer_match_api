class SessionsController < ApplicationController
  before_filter :already_authorized!, only: %i(new create)

  def new
  end

  def create
    user = Authenticator.new.sign_in_with_facebook(params[:facebook_token])

    unless user
      render :new and return
    end

    unless user.organization
      redirect_to new_registration_path and return
    end

    sign_in user

    redirect_to opportunities_path and return
  end

  def destroy
    sign_out current_user

    redirect_to home_organizations_path
  end
end
