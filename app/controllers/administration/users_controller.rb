module Administration
  class UsersController < Administration::ApplicationController

    def index
      @users = User.with_deleted.order(created_at: :desc).page(params[:page])
    end

    def destroy
      User.with_deleted.find(params[:id]).really_destroy!

      redirect_to action: :index
    end
  end
end
