module Administration
  class ApplicationController < ::ApplicationController
    before_action :authenticate_admin!

    layout 'administration'

  end
end
