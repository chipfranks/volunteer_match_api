module Administration
  class SessionsController < ::Devise::SessionsController
    layout 'administration'

    def after_sign_in_path_for(resource_or_scope)
      administration_path
    end

    def after_sign_out_path_for(resource_or_scope)
      root_path
    end
  end
end

