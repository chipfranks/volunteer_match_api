module Administration
  class OpportunitiesController < Administration::ApplicationController

    def index
      @opportunities = Opportunity.with_deleted.order(created_at: :desc).page(params[:page])
    end

    def destroy
      Opportunity.with_deleted.find(params[:id]).really_destroy!

      redirect_to action: :index
    end
  end
end
