class RegistrationsController < ApplicationController
  before_filter :already_authorized!

  def new
    @serialized_organization = serialized(Organization.new)
  end

  def create
    user = Authenticator.new.sign_up_organization_with_facebook(organization_params, params[:facebook_token])

    if user
      sign_in user
      redirect_to opportunities_path and return
    end

    @serialized_organization = serialized(Organization.new(organization_params))

    render :new

  rescue Authenticator::UserAlreadyRegisteredWithFacebook
    user = Authenticator.new.sign_in_with_facebook(params[:facebook_token])
    sign_in user

    redirect_to opportunities_path
  end

  private

  def organization_attributes
    %i(name image website_url address)
  end

  def organization_params
    params.require(:organization).permit organization_attributes
  end
end
