class PanelController < ApplicationController
  include Pundit

  before_filter :authenticate_user!

  layout 'organization'

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  protected

  def user_not_authorized
    redirect_to opportunities_path
  end
end
