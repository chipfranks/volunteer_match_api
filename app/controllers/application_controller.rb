class ApplicationController < ActionController::Base
  include Pundit

  def serialized(obj, opts={})
    if obj.is_a?(ActiveRecord::Base) ||
        obj.is_a?(ActiveRecord::AssociationRelation) ||
        obj.is_a?(ActiveRecord::Associations::CollectionProxy)

      ActiveModel::SerializableResource.new(obj, opts).serializable_hash
    else
      obj.as_json
    end
  end

  def already_authorized!
    if user_signed_in?
      redirect_to opportunities_path and return
    end
  end
end
