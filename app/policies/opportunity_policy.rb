class OpportunityPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def join?
    record.status == 'Published'
  end

  def leave?
    true
  end

  def manage?
    user.organization && user.organization.id == record.organization_id
  end

  def close?
    manage?
  end

  def show?
    manage?
  end

  def create?
    true
  end

  def edit?
    manage?
  end

  def update?
    manage?
  end

  def destroy?
    manage?
  end
end
