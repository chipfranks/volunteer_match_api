window.ImageDrawer =
  drawImage: (source, canvas) ->
    switch typeof source
      when 'object'
        src = URL.createObjectURL(source) if source
      when 'string'
        src = source

    ctx = canvas.getContext('2d')
    ctx.clearRect(0, 0, 68, 68)

    if src
      img = new Image()
      img.onload = ->
        imgW = @width
        imgH = @height
        if imgW > imgH
          scaleY = imgW / imgH
          canvasH = 68
          canvasW = scaleY * 68
          canvasY = 0
          canvasX = (68 - canvasW) / 2
        else
          scaleX = imgH / imgW
          canvasH = scaleX * 68
          canvasW = 68
          canvasY = (68 - canvasH) / 2
          canvasX = 0
        ctx.drawImage(img, 0, 0, imgW, imgH, canvasX, canvasY, canvasW, canvasH)
      img.src = src