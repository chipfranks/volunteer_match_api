window.DebugStore =
  pubsub: false

window.PubSubHelper =
  componentWillMount: ->
    @subscribers = []

  componentWillUnmount: ->
    @unsubscribeAll()

  subscribe: (event, fn) ->
    @debugPubSub('Subscribed', event, fn, this)

    @subscribers.push PubSub.subscribe(event, fn)

  subscribeOnce: (event, fn) ->
    @debugPubSub('SubscribedOnce', event, fn, this)

    self = this
    once = PubSub.subscribe event, (event_name, data) ->
      fn.call(event_name, data)
      self.unsubscribe(once)

  publish: (event, data) ->
    @debugPubSub('Published', event, data, this)
    PubSub.publishSync event, data

  unsubscribe: (subscriber) ->
    PubSub.unsubscribe subscriber
    @subscribers = @subscribers.filter (s) -> s != subscriber

  unsubscribeAll: ->
    PubSub.unsubscribe(subscriber) for subscriber in @subscribers
    @subscribers = []

  debugPubSub: (type, event_name, payload, self) ->
    return unless DebugStore.pubsub
    if DebugStore.pubsub.ignore
      ignore = false
      DebugStore.pubsub.ignore.forEach (ignore_rule) ->
        if event_name.match(ignore_rule)?
          ignore = true
      return if ignore

    colors = { 'Subscribed': '#a0a', 'Published': '#0a0' }
    style = "color: #{colors[type]};font-weight:bold"

    console.groupCollapsed(type + ": " + event_name)
    console.log "Stack", (new Error).stack
    console.log "Payload", payload
    console.log "This", self
    console.groupEnd()