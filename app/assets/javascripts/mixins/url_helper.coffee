window.UrlHelper =

  getQueryVariable: (variable) ->
    query = window.location.search.substring(1)
    variables = query.split('&')

    for v in variables
      pair = v.split('=')
      return pair[1] if pair[0] == variable

    null