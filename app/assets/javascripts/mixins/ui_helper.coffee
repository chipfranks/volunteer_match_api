window.UiHelper =

  createCategorySelect: (callback) ->
    $('.select__category').each ->

      element = @

      $this = $(element)
        .addClass('hidden')
        .removeClass('select__category')
        .wrap('<div class="select__category"></div>')
        .after('<div class="select__category-field"></div>')

      $options = $this.children('option')

      $field = $this.next('.select__category-field')
      category = $options.first().text()
      categoryClass = category.split('/')[0].toLowerCase()
      $field.html "<span class='icon icon__#{categoryClass}--sm'></span>#{category}"

      $list = $('<ul />', 'class': 'select__category-list').insertAfter($field)

      $options.each ->
        $el = $(@)
        categoryClass = $el.text().split('/')[0].toLowerCase()
        $('<li />',
          html: "<span class='icon icon__#{categoryClass}--sm'></span>#{$el.text()}"
          rel: $el.val()
        ).appendTo($list)

      $listItems = $list.children('li')

      $field.click (event) ->
        event.stopPropagation()
        $('.select__category-field.active').each ->
          $(@).removeClass('active').next('.select__category-list').hide()
        $(@).toggleClass('active').next('.select__category-list').toggle()

      $listItems.click (event) ->
        event.stopPropagation()
        $field.html($(@).html()).removeClass 'active'
        $this.val($(@).attr('rel'))
        $list.hide()
        callback.call(@, event)

      $(document).click ->
        $field.removeClass 'active'
        $list.hide()