window.ValidationHelper =

  hasError: (field) ->
    !!@state.errors[field]

  hasSuccess: (field) ->
    @state.errors.hasOwnProperty(field) && !@hasError(field)

  validateField: (validator, showMessage) ->
    result = validate({
      "#{validator.field}": $(validator.selector).val()
    },{
      "#{validator.field}": validator.validations
    })

    errors = @state.errors

    if showMessage
      errors[validator.field] = if result && result[validator.field] then result[validator.field].join(' / ').toLocaleLowerCase() else null
#    else
#      errors[validator.field] = null if $(validator.selector).val()

    @setState(errors: errors)

    !(result && result[validator.field])