window.Pluralizer =

  pluralize: (text, count, text2) ->
    return text if count < 2
    return text2 if text2

    text + 's'
