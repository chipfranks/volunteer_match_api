window.GoogleMapsHelper =

  initAutocomplete: (element, callback) ->
    # Create the autocomplete object, restricting the search to geographical
    # location types.
    @autocomplete = new google.maps.places.Autocomplete(
      element, {
        types: ['geocode']
      })

    # When the user selects an address from the dropdown, populate the address
    # fields in the form.
    @autocomplete.addListener('place_changed', callback)

    # Bias the autocomplete object to the user's geographical location,
    # as supplied by the browser's 'navigator.geolocation' object.
    # @geolocate()

  initMap: (element, center, category, zoomControl) ->
    unless center
      center =
        lat: 30.2725452
        lng: -97.7421601
    map = new google.maps.Map(
      element, {
        center: center
        zoom: 16
        mapTypeId: google.maps.MapTypeId.ROADMAP
        zoomControl: zoomControl
        mapTypeControl: false
        scaleControl: false
        streetViewControl: false
        rotateControl: false
        fullscreenControl: false
        scrollwheel: false
      })
    @marker = new google.maps.Marker
      position: center
      map: map
      icon: "/icons/map/#{category}.svg"
    map

  redrawMap: (map, coords, category) ->
    @refreshMap(map)
    map.setCenter(coords)
    @marker.setPosition(coords)
    @marker.setIcon("/icons/map/#{category}.svg")

  refreshMap: (map) ->
    google.maps.event.trigger(map, 'resize')

  geolocate: ->
    if navigator.geolocation
      navigator.geolocation.getCurrentPosition (position) =>
        @geolocation =
          lat: position.coords.latitude
          lng: position.coords.longitude
        circle = new google.maps.Circle
          center: @geolocation
          radius: position.coords.accuracy
        @autocomplete.setBounds(circle.getBounds())