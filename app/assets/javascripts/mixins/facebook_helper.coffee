window.FacebookHelper =
  loadFbSDK: ->
    window.fbAsyncInit = =>
      FB.init
        appId: '1710561099157699'
        version: 'v2.5'
      @checkLoginState()

    # Load FB SDK asynchronously
    ((d, s, id) ->
      js = undefined
      fjs = d.getElementsByTagName(s)[0]
      if d.getElementById(id)
        return
      js = d.createElement(s)
      js.id = id
      js.src = '//connect.facebook.net/en_US/sdk.js'
      fjs.parentNode.insertBefore js, fjs
      return
    ) document, 'script', 'facebook-jssdk'

  statusSetCallback: (response) ->
    @publish(actions.FACEBOOK_STATUS_SET, response)

  statusChangeCallback: (response) ->
    @publish(actions.FACEBOOK_STATUS_CHANGED, response)

  logIn: ->
    FB.login((response) =>
      @statusChangeCallback(response)
    , {
      scope: 'public_profile,email'
    })

  checkLoginState: ->
    FB.getLoginStatus (response) =>
      @statusSetCallback(response)