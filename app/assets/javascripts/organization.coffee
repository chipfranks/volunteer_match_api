#= require jquery
#= require jquery_ujs
#= require react
#= require react_ujs
#= require js-routes
#= require bootstrap
#= require marked.min
#= require vue
#= require pubsub
#= require moment
#= require validate
#= require validate_functions
#= require lib/underscore_custom
#= require autosize
#= require classNames
#= require jsonapi-datastore
#= require lib/daterangepicker_custom

#= require_tree ./mixins

#= require ./components/actions
#= require ./components/markdown_preview

#= require_tree ./components/opportunity/tabs
#= require_tree ./components/opportunity/hints

#= require ./components/opportunity/actions
#= require ./components/opportunity/statistic
#= require ./components/opportunity/preview
#= require ./components/opportunity/form
#= require ./components/opportunity/help
#= require ./components/opportunity/editor
#= require ./components/opportunity/list

#= require_tree ./components/settings/tabs
#= require_tree ./components/settings/hints

#= require ./components/settings/form
#= require ./components/settings/help
#= require ./components/settings/editor
