validate.extend validate.validators.datetime,
  parse: (value, options) ->
    moment.utc(value)
  format: (value, options) ->
    format = if options.dateOnly then 'YYYY-MM-DD' else 'YYYY-MM-DD hh:mm:ss'
    moment.utc(value).format format
