#= require jquery
#= require jquery_ujs
#= require scrollreveal
#= require bootstrap
#= require react
#= require react_ujs
#= require js-routes
#= require pubsub
#= require validate
#= require validate_functions
#= require classNames
#= require jsonapi-datastore

#= require_tree ./mixins

#= require ./components/actions
#= require ./components/session/form
#= require ./components/registration/form

#= require ./components/video/modal
#= require ./components/video/play_button

$(document).on 'ready', ->

  # disable phrasing
  $('span[contenteditable]').removeAttr('contenteditable')

  window.sr = ScrollReveal({
    distance: 0
    opacity: 0
    duration: 1000
    scale: 1
    mobile: false
  })

  if $('.animate-scroll-fade-in-offset-top-1').length
    sr.reveal('.animate-scroll-fade-in-offset-top-1', {
      reset: true
      distance: '60px'
    })

  if $('.animate-scroll-fade-in-offset-top-2').length
    sr.reveal('.animate-scroll-fade-in-offset-top-2', {
      reset: true
      distance: '90px'
    })

  $('a[data-toggle="tab"]').on 'shown.bs.tab', ->
    sr.sync()

  $('.page__nav').on 'click', (event) ->
    $this = $(event.target)
    href  = $this.attr('href')

    if href && href.indexOf('#') == 0 && $(href).length
      event.preventDefault()
      $('html, body').animate({ scrollTop: $(href).offset().top }, 500)
