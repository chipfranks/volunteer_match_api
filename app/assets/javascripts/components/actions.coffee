window.actions =

  OPPORTUNITY_EDITOR_OPPORTUNITY_CHANGED: 'opportunity.editor.opportunity_changed'
  OPPORTUNITY_EDITOR_TAB_CHANGED:         'opportunity.editor.tab_changed'
  OPPORTUNITY_EDITOR_TAB_VALID:           'opportunity.editor.tab_valid'
  OPPORTUNITY_EDITOR_SAVE_CLICKED:        'opportunity.editor.save_clicked'
  OPPORTUNITY_EDITOR_HINT_TOGGLE:         'opportunity.editor.hint_toggle'

  ORGANIZATION_EDITOR_TAB_CHANGED:        'organization.editor.tab_changed'
  ORGANIZATION_EDITOR_HINT_TOGGLE:        'organization.editor.hint_toggle'

  FACEBOOK_STATUS_SET:                    'facebook.status.set'
  FACEBOOK_STATUS_CHANGED:                'facebook.status.changed'

  VIDEO_PLAY_BUTTON_CLICKED:              'video.play_button.clicked'