DOM = React.DOM

RegistrationForm = React.createClass
  displayName: 'RegistrationForm'
  mixins: [PubSubHelper, FacebookHelper, GoogleMapsHelper, ImageDrawer, ValidationHelper]


  getInitialState: ->
    {
      organization: (new JsonApiDataStore()).sync(@props.organization)
      facebookToken: ''
      userName: ''
      userPicture: ''
      valid: false
      errors: {}
      validators:
        name:
          field: 'name'
          selector: '#organization__name'
          validations:
            presence: true
        address:
          field: 'address'
          selector: '#organization__address'
          validations:
            presence: true
        website_url:
          field: 'website_url'
          selector: '#organization__website_url'
          validations:
            format:
              pattern: /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/
              message: -> 'is invalid'
    }


  componentDidMount: ->
    @loadFbSDK()

    @subscribe actions.FACEBOOK_STATUS_CHANGED, (event_name, response) =>
      if response.status == 'connected'
        @setState(facebookToken: response.authResponse.accessToken)
        @getUser()
      else
        @setState
          facebookToken: ''
          userName: ''
          userPicture: ''

    searchBox = $('#organization__address')[0]

    @initAutocomplete(searchBox, @onSearch)

    @drawImage(@state.organization.image, $("#organization__image")[0])
    @validate()



  onSearch: ->
    place = @autocomplete.getPlace()
    address = place.formatted_address

    organization = @state.organization
    organization.address = address

    @setState(organization: organization)


  onImageChange: (event) ->
    $input  = $(event.target)
    file    = $input[0].files[0]

    unless file.type.match(/image.*/)
      $input.val('')
      return

    canvas = $("##{$input.data('canvas')}")[0]

    organization = @state.organization
    organization.image = file

    @setState(organization: organization)

    @drawImage(file, canvas)


  onNameChange: (event) ->
    value = $(event.target).val()

    organization = @state.organization
    organization.name = value

    @validateField(@state.validators.name, true)

    @setState(organization: organization)
    @validate()


  onAddressChange: (event) ->
    value = $(event.target).val()

    organization = @state.organization
    organization.address = value

    @validateField(@state.validators.address, true)

    @setState(organization: organization)
    @validate()


  onWebsiteUrlChange: (event) ->
    value = $(event.target).val()

    organization = @state.organization
    organization.website_url = value

    @validateField(@state.validators.website_url, true)

    @setState(organization: organization)
    @validate()


  validate: (showMessages) ->
    showMessages ||= false

    valid = @validateField(@state.validators.name, showMessages)
    valid = @validateField(@state.validators.address, showMessages) && valid
    valid = @validateField(@state.validators.website_url, showMessages) && valid

    @setState(valid: valid)

    valid


  getUser: ->
    FB.api(
      '/me?fields=first_name,picture',
      (response) =>
        @setState
          userName:     response.first_name
          userPicture:  response.picture.data.url
    )


  onSubmit: (event) ->
    if @state.facebookToken
      # do nothing - the form will be submited
    else
      event.preventDefault()
      @logIn()


  render: ->
    DOM.form
      id: 'registration-form'
      encType: 'multipart/form-data'
      action: Routes.new_registration_path()
      method: 'POST'
      acceptCharset: 'UTF-8'
      autoComplete: 'off'
      onSubmit: @onSubmit
      DOM.div
        className: 'tab-content'
        DOM.div
          className: "tab-pane tab-pane-white tab-pane--block regular active"
          DOM.h3
            className: 'hk__grotesk-light text-center'
            'Organization'
          DOM.div
            className: 'tab-pane__content'
            DOM.div
              className: 'form-group text-center'
              DOM.label
                className: 'control-label canvas__preview-label'
                DOM.canvas
                  id: 'organization__image'
                  className: 'canvas__preview'
                  width: '68'
                  height: '68'
                DOM.input
                  className: 'hidden'
                  name: 'organization[image]'
                  type: 'file'
                  'data-canvas': 'organization__image'
                  onChange: @onImageChange
                '+ Add photo'
          DOM.div
            className: classNames(
              'form-group':   true
              'has-error':    @hasError('name')
              'has-success':  @hasSuccess('name')
            )
            DOM.label
              htmlFor: 'organization__name'
              className: 'control-label required'
              'Name'
              if @hasError('name')
                DOM.span
                  className: 'form__error pull-right'
                  @state.errors.name
            DOM.input
              id: 'organization__name'
              type: 'text'
              className: 'form-control form__control-huge'
              name: 'organization[name]'
              onChange: @onNameChange
              placeholder: 'Add name of the organization'
              value: @state.organization.name
          DOM.div
            className: classNames(
              'form-group': true
              'has-error': @hasError('address')
              'has-success': @hasSuccess('address')
            )
            DOM.label
              htmlFor: 'organization__address'
              className: 'control-label required'
              'Address'
              if @hasError('address')
                DOM.span
                  className: 'form__error pull-right'
                  @state.errors.address
              DOM.input
                id: 'organization__address'
                className: 'form-control'
                name: 'organization[address]'
                onChange: @onAddressChange
                placeholder: '86 Bins Heights Suite 359'
                value: @state.organization.address
          DOM.div
            className: classNames(
              'form-group':   true
              'has-error':    @hasError('website_url')
              'has-success':  @hasSuccess('website_url')
            )
            DOM.label
              htmlFor: 'organization__website_url'
              className: 'control-label'
              'Website'
              if @hasError('website_url')
                DOM.span
                  className: 'form__error pull-right'
                  @state.errors.website_url
            DOM.input
              id: 'organization__website_url'
              type: 'text'
              className: 'form-control'
              name: 'organization[website_url]'
              onChange: @onWebsiteUrlChange
              placeholder: 'Add website url of the organization'
              value: @state.organization.website_url
          DOM.input
            type: 'hidden'
            name: 'facebook_token'
            value: @state.facebookToken
          DOM.div
            className: 'form-group text-center'
            DOM.button
              type: 'submit'
              className: classNames(
                'btn btn-fb btn-lg': true
                'has-avatar': @state.userPicture
              )
              disabled: !@state.valid
              if @state.userName
                DOM.span
                  className: ''
                  DOM.span
                    className: 'avatar__preview'
                    style:
                      backgroundImage: "url(#{@state.userPicture})"
                  DOM.span
                    "Continue as "
                  DOM.span
                    className: 'text__gray--light'
                    @state.userName
              else
                DOM.span
                  className: 'text-uppercase'
                  'Sign up with Facebook'
          DOM.div
            className: 'form-group text-center'
            DOM.p
              className: 'text__gray--medium-dark'
              'By registering you accept with our '
              DOM.a
                className: 'text__gray--medium-dark'
                href: '#'
                'Terms of Use'
              ' and '
              DOM.a
                className: 'text__gray--medium-dark'
                href: '#'
                'Privacy Policy'
              '.'



window.RegistrationForm = React.createFactory(RegistrationForm)