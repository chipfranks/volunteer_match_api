DOM = React.DOM

OpportunityActions = React.createClass
  displayName: 'OpportunityActions'
  mixins: [PubSubHelper]

  
  onSaveClick: (event) ->
    event.preventDefault()

    @publish(actions.OPPORTUNITY_EDITOR_SAVE_CLICKED)


  render: ->
    DOM.div
      className: ''
      DOM.button
        className: 'btn btn-circle'
        onClick: @onSaveClick
        'Save'

window.OpportunityActions = React.createFactory(OpportunityActions)