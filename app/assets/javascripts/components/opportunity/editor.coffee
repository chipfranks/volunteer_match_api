DOM = React.DOM

OpportunityEditor = React.createClass
  displayName: 'OpportunityEditor'
  mixins: [PubSubHelper]


  getInitialState: ->
    {
      opportunity: (new JsonApiDataStore()).sync(@props.opportunity)
      contacts: (new JsonApiDataStore()).sync(@props.contacts)
    }


  componentDidMount: ->
    @subscribe actions.OPPORTUNITY_EDITOR_OPPORTUNITY_CHANGED, (event_name, opportunity) =>
      @setState(opportunity: opportunity)

    @subscribe actions.OPPORTUNITY_EDITOR_HINT_TOGGLE, (event_name) =>
      $('.modal-hint').fadeToggle(500)


  render: ->
    DOM.div
      className: 'row'
      DOM.div
        className: 'col-md-6 col-md-offset-0 col-sm-10 col-sm-offset-1'
        OpportunityForm
          opportunity: @state.opportunity
          contacts: @state.contacts
          categories: @props.categories
          url: @props.url
          http_method: @props.http_method
      DOM.div
        className: 'col-md-6 col-md-offset-0 col-sm-10 col-sm-offset-1 modal-hint'
        OpportunityHelp
          opportunity: @state.opportunity
          categories: @props.categories

window.OpportunityEditor = React.createFactory(OpportunityEditor)