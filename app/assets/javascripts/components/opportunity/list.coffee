DOM = React.DOM

OpportunityList = React.createClass
  displayName: 'OpportunityList'
  mixins: [PubSubHelper, Pluralizer, UrlHelper, FacebookHelper]


  getInitialState: ->
    groupAttribute = @getQueryVariable('sort_by') || 'start_date'

    opportunities = (new JsonApiDataStore()).sync(@props.opportunities)
    opportunity_groups = _.groupBy(opportunities, (o) ->
      date = moment(o[groupAttribute])
      date.format('MMMM YYYY')
    )

    {
      opportunities:      opportunities
      opportunity_groups: opportunity_groups
      groupAttribute:     groupAttribute
      currentPage:        1
      allLoaded:          (opportunities.length < 25)
    }


  componentDidMount: ->
    @loadFbSDK()


  getCategoryClassFromId: (category) ->
    category = _.invert(@props.categories)[category]
    category.split('/')[0].toLowerCase()


  getStatusFromId: (status) ->
    _.invert(@props.statuses)[status]


  getDateRange: (start, end) ->
    start = moment(start).format('MMM DD')
    end = moment(end).format('MMM DD')

    [start, end].join(' - ')


  getUsersCount: (opportunity) ->
    opportunity.users.length


  toggleMenu: (event) ->
    event.stopPropagation()

    $button = $(event.target)

    tableId = $button.parents('.table').attr('id')

    $other = $(".table__actions:not([data-table='#{tableId}'])").removeClass('active')
    setTimeout((-> $other.hide()), 300)

    $actions = $(".table__actions[data-table='#{tableId}']")

    if $actions.is('.active')
      $actions.removeClass('active')
      setTimeout((-> $actions.hide()), 300)
    else
      $actions.show()
      setTimeout((-> $actions.addClass('active')), 100)


  loadOpportunities: (event) ->
    $button = $(event.target)
    $button.attr('disabled', 'disabled')
    $button.text('Loading...')

    organization_id = @state.opportunities[0].organization.id

    query = {
      'page[number]':   @state.currentPage + 1
      'page[size]':     25
      order:            'start_date:desc'
      includes:         'users'
    }

    query.sort_by     = "#{@getQueryVariable('sort_by')}:desc" if @getQueryVariable('sort_by')
    query.with_status = @getQueryVariable('with_status') if @getQueryVariable('with_status')

    $.ajax(
      url:      Routes.api_organization_opportunities_path(organization_id)
      data:     query
      method:   'get'
      dataType: 'json'
    )
    .done((data) =>
      opportunities = _.union(@state.opportunities, (new JsonApiDataStore()).sync(data))
      opportunity_groups = _.groupBy(opportunities, (o) =>
        date = moment(o[@.state.groupAttribute])
        date.format('MMMM YYYY')
      )

      @setState(
        allLoaded:          !data.links.next
        currentPage:        query['page[number]']
        opportunities:      opportunities
        opportunity_groups: opportunity_groups
      )

      $button.removeAttr('disabled')
      $button.text('Load more')
    )
    .error((err) =>
      console.log err

      $button.attr('disabled')
      $button.text('Load more')
    )


  share: (opportunity) ->
    event.preventDefault()

    FB.ui
      method: 'share'
      display: 'popup'
      mobile_iframe: 'false'
      href: 'http://joevolunteer.com/'
#      picture: ''
      description: "I’m volunteering to help others, this time at #{opportunity.title} — I don’t say this to brag, but I want more people to do this, too. Please check out the JoeVolunteer App and help us all spread Acts of Human Kindness throughout the world! Thank you."
    , (response) =>
      console.log response


  render: ->
    if _.keys(@state.opportunity_groups).length < 1
      DOM.h2
        className: 'hk__grotesk-light text-center'
        'No opportunities found'
    else
      DOM.div
        className: ''
        for month, opportunities of @state.opportunity_groups
          monthId = month.replace(' ', '')

          DOM.div
            key: "opportunity-list-#{monthId}"
            DOM.p
              className: 'hk__grotesk-medium table__title'
              month
            DOM.div
              className: 'tab-pane tab-pane-white tab-pane--list'
              for opportunity, i in opportunities
                DOM.div
                  className: 'tab-pane--list-item'
                  key: "opportunity-list-#{monthId}-row-#{i}"
                  if opportunity.status != 2
                    DOM.div
                      className: 'table__actions'
                      'data-table': "table--#{monthId + i}"
                      DOM.table
                        className: 'table'
                        DOM.tbody
                          className: ''
                          DOM.tr
                            className: ''
                            DOM.td
                              className: 'text-right'
                              DOM.a
                                className: 'table__action-link text__gray--base hk__grotesk-medium text-center'
                                href: Routes.close_opportunity_path(opportunity.id)
                                'data-method': 'post'
                                DOM.span
                                  className: 'icon icon__close'
                                DOM.br {}
                                'Close'
                              DOM.a
                                className: 'table__action-link text__gray--base hk__grotesk-medium text-center'
                                href: Routes.edit_opportunity_path(opportunity.id)
                                DOM.span
                                  className: 'icon icon__edit'
                                DOM.br {}
                                'Edit'
                              DOM.a
                                className: 'table__action-link text__gray--base hk__grotesk-medium text-center'
                                href: '#'
                                onClick: => @share(opportunity)
                                DOM.span
                                  className: 'icon icon__share--sm'
                                DOM.br {}
                                'Share'
                  DOM.table
                    id: "table--#{monthId + i}"
                    className: 'table table--opportunities'
                    DOM.tbody
                      className: ''
                      DOM.tr
                        className: classNames(
                          active: opportunity.status == 1 # opportunity is published
                        )
                        DOM.td
                          className: ''
                          DOM.a
                            href: Routes.opportunity_path(opportunity.id)
                            DOM.div
                              className: 'preview__image-container'
                              if opportunity.image
                                DOM.div
                                  className: ''
                                  DOM.div
                                    className: 'background__preview'
                                    style:
                                      backgroundImage: "url(#{opportunity.image})"
                                  DOM.span
                                    className: "preview__category-icon icon icon__#{@getCategoryClassFromId(opportunity.category)}--sm"
                              else
                                DOM.span
                                  className: "preview__category-plug icon icon__#{@getCategoryClassFromId(opportunity.category)}"
                        DOM.td
                          className: ''
                          DOM.a
                            href: Routes.opportunity_path(opportunity.id)
                            DOM.h4
                              className: 'hk__grotesk-bold'
                              opportunity.title
                            DOM.p
                              className: ''
                              _.truncate(opportunity.summary, 120)
                        DOM.td
                          className: 'text-center'
                          DOM.a
                            href: Routes.opportunity_path(opportunity.id)
                            if @getUsersCount(opportunity)
                              DOM.div
                                className: ''
                                if @getUsersCount(opportunity) > 3
                                  DOM.p
                                    className: ''
                                    "+ #{@getUsersCount(opportunity) - 3} #{@pluralize('Joe', @getUsersCount(opportunity) - 3, 'Joe\'s')}"
                                for user, j in _.first(opportunity.users, 3)
                                  DOM.div
                                    key: "opportunity-list-#{monthId}-row-#{i}-user-#{j}"
                                    className: 'avatar__preview'
                                    style:
                                      backgroundImage: "url(#{user.image})"
                            else
                              DOM.p
                                className: ''
                                "No Joe's yet"
                        DOM.td
                          className: 'text-center'
                          DOM.a
                            href: Routes.opportunity_path(opportunity.id)
                            DOM.p
                              className: classNames(
                                'text__blue--light':        opportunity.status == 1
                                'text__gray--medium-dark':  opportunity.status != 1
                              )
                              @getStatusFromId(opportunity.status)
                            DOM.p
                              className: ''
                              @getDateRange(opportunity.start_date, opportunity.end_date)
                        DOM.td
                          className: 'text-center'
                          if opportunity.status != 2
                            DOM.button
                              className: 'btn btn-transparent'
                              onClick: @toggleMenu
                              DOM.span
                                className: 'icon icon__options'
        unless @state.allLoaded
          DOM.div
            className: 'text-center'
            DOM.button
              className: 'btn btn-circle btn-circle-white btn-lg'
              onClick: @loadOpportunities
              'Load more'


window.OpportunityList = React.createFactory(OpportunityList)