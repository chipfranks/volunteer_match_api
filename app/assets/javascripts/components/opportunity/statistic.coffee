DOM = React.DOM

OpportunityStatistic = React.createClass
  displayName: 'OpportunityStatistic'
  mixins: [PubSubHelper, Pluralizer, FacebookHelper]


  getInitialState: ->
    opportunity = @props.opportunity
    unless opportunity instanceof JsonApiDataStoreModel
      opportunity = (new JsonApiDataStore()).sync(opportunity)

    user_groups = _.toArray(_.groupBy(opportunity.users, (u, i) ->
      Math.floor(i / 2)
    ))

    {
      opportunity: opportunity
      user_groups: user_groups
    }


  componentDidMount: ->
    @loadFbSDK()


  getUserName: (first_name, last_name) ->
    _.truncate([first_name, last_name].join(' '), 15)


  share: (event) ->
    event.preventDefault()

    FB.ui
      method: 'share'
      display: 'popup'
      mobile_iframe: 'false'
      href: 'http://joevolunteer.com/'
#      picture: ''
      description: "I’m volunteering to help others, this time at #{@state.opportunity.title} — I don’t say this to brag, but I want more people to do this, too. Please check out the JoeVolunteer App and help us all spread Acts of Human Kindness throughout the world! Thank you."
    , (response) =>
      console.log response


  render: ->
    DOM.div
      className: ''
      DOM.p
        className: 'hk__grotesk-medium table__title'
        'Statistic'
      DOM.div
        className: "tab-pane tab-pane-white tab-pane--block active"
        DOM.div
          className: 'tab-pane__content'
          DOM.h2
            className: 'hk__grotesk-light text-center'
            "#{@state.opportunity.users.length} #{@pluralize('Volunteer', @state.opportunity.users.length)}"
          DOM.table
            className: 'table table--users'
            DOM.tbody
              className: ''
              for user_group, i in @state.user_groups
                DOM.tr
                  key: "opportunity-user-group-#{i}"
                  for user, j in user_group
                    DOM.td
                      key: "opportunity-user-#{j}-group-#{i}"
                      className: ''
                      if user.facebook_uid
                        DOM.a
                          href: "https://www.facebook.com/#{user.facebook_uid}"
                          className: 'text__gray--base'
                          DOM.div
                            className: 'avatar__preview'
                            style:
                              backgroundImage: "url(#{user.image})"
                          DOM.span
                            className: 'hk__grotesk-medium'
                            @getUserName(user.first_name, user.last_name)
                      else if user.twitter_uid
                        DOM.a
                          href: "https://twitter.com/intent/user?user_id=#{user.twitter_uid}"
                          className: 'text__gray--base'
                          DOM.div
                            className: 'avatar__preview'
                            style:
                              backgroundImage: "url(#{user.image})"
                          DOM.span
                            className: 'hk__grotesk-medium'
                            @getUserName(user.first_name, user.last_name)
          if @state.opportunity.status != 2 # not closed
            DOM.div
              className: 'tab-pane__actions text-center'
              DOM.div
                className: 'form-group'
                DOM.button
                  className: 'btn btn-fb btn-lg text-uppercase btn-fb-inverse'
                  onClick: @share
                  'Share on Facebook'


window.OpportunityStatistic = React.createFactory(OpportunityStatistic)