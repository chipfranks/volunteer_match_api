DOM = React.DOM

OpportunityPublishHint = React.createClass
  displayName: 'OpportunityPublishHint'
  mixins: [PubSubHelper]

  getInitialState: ->
    {
      opportunity: @props.opportunity
    }


  componentDidMount: ->
    @subscribe actions.OPPORTUNITY_EDITOR_OPPORTUNITY_CHANGED, (event_name, opportunity) =>
      @setState(opportunity: opportunity)


  render: ->
    DOM.div
      id: 'opportunity__hint-publish'
      className: "tab-pane tab-pane-white tab-pane--iphone #{'active' if @props.active}"
      DOM.div
        className: 'tab-pane__content'
        OpportunityPreview(opportunity: @state.opportunity, categories: @props.categories)

window.OpportunityPublishHint = React.createFactory(OpportunityPublishHint)
