DOM = React.DOM

OpportunityAddressHint = React.createClass
  displayName: 'OpportunityAddressHint'

  render: ->
    DOM.div
      id: 'opportunity__hint-address'
      className: "tab-pane tab-pane-white tab-pane--hint #{'active' if @props.active}"
      DOM.div
        className: 'tab-pane__content'
        DOM.div
          className: 'hint__block'
          DOM.span
            className: 'icon icon__book'
        DOM.div
          className: 'hint__block'
          DOM.h3
            className: 'hk__grotesk-bold'
            'Search address'
          DOM.p
            className: ''
            'The Opportunity Description should be a brief and straightforward explanation of what the volunteer opportunity entails. Make the description enticing by describing the opportunity impact on the community and the constituents served. Remember to proofread your listing - selecting the ABC check box will spell check the description for you!'

window.OpportunityAddressHint = React.createFactory(OpportunityAddressHint)
