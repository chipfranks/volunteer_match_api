DOM = React.DOM

OpportunityForm = React.createClass
  displayName: 'OpportunityForm'
  mixins: [PubSubHelper]


  getInitialState: ->
    {
      currentTab: 'about'
      validationStatuses:
        'about': false
        'description': false
        'contact': false
        'about': false
    }


  componentDidMount: ->
    @subscribe actions.OPPORTUNITY_EDITOR_TAB_CHANGED, (event_name, tab) =>
      @setState(currentTab: tab)

    @subscribe actions.OPPORTUNITY_EDITOR_TAB_VALID, (event_name, data) =>
      validationStatuses = @state.validationStatuses

      validationStatuses[data.tab] = data.valid

      @setState(validationStatuses: validationStatuses)


  onTabClick: (event) ->
    event.preventDefault()

    target    = $(event.target).attr('href').replace('#opportunity__tab-', '')
    disabled  = $(event.target).parent().is('.disabled')

    unless disabled
      @publish(actions.OPPORTUNITY_EDITOR_TAB_CHANGED, target)


  onSubmit: (event) ->
    unless @state.currentTab == 'publish'
      event.preventDefault()


  toggleHint: (event) ->
    $this = $(event.target)
    $this.toggleClass('icon__book--red').toggleClass('icon__dismiss--red')

    @publish(actions.OPPORTUNITY_EDITOR_HINT_TOGGLE)


  render: ->
    DOM.form
      id: 'opportunity-form'
      encType: 'multipart/form-data'
      action: @props.url
      method: 'POST'
      acceptCharset: 'UTF-8'
      onSubmit: @onSubmit
      autoComplete: 'off'
      DOM.input
        name: '_method'
        type: 'hidden'
        value: @props.http_method
      DOM.a
        href: '#'
        className: 'icon icon__book--red modal-hint__toggle'
        onClick: @toggleHint
      DOM.div
        DOM.ul
          className: 'nav nav-tabs nav-justified'
          DOM.li
            className: classNames({
              active: @state.currentTab == 'about'
              disabled: false
            })
            DOM.a
              href: '#opportunity__tab-about'
              onClick: @onTabClick
              'About'
          DOM.li
            className: classNames({
              active: @state.currentTab == 'description'
              disabled: !@state.validationStatuses.about
            })
            DOM.a
              href: '#opportunity__tab-description'
              onClick: @onTabClick
              'Description'
          DOM.li
            className: classNames({
              active: @state.currentTab == 'contact'
              disabled: !(@state.validationStatuses.about && @state.validationStatuses.description)
            })
            DOM.a
              href: '#opportunity__tab-contact'
              onClick: @onTabClick
              'Contact'
          DOM.li
            className: classNames({
              active: @state.currentTab == 'address'
              disabled: !(@state.validationStatuses.about && @state.validationStatuses.description && @state.validationStatuses.contact)
            })
            DOM.a
              href: '#opportunity__tab-address'
              onClick: @onTabClick
              'Address'
          DOM.li
            className: classNames({
              active: @state.currentTab == 'publish'
              disabled: !(@state.validationStatuses.about && @state.validationStatuses.description && @state.validationStatuses.contact && @state.validationStatuses.address)
            })
            DOM.a
              href: '#opportunity__tab-publish'
              onClick: @onTabClick
              'Publish'
        DOM.div
          className: 'tab-content'
          OpportunityAboutTab
            active: @state.currentTab == 'about'
            opportunity: @props.opportunity
            categories: @props.categories
          OpportunityDescriptionTab
            active: @state.currentTab == 'description'
            opportunity: @props.opportunity
          OpportunityContactTab
            active: @state.currentTab == 'contact'
            opportunity: @props.opportunity
            contacts: @props.contacts
            contact: (@props.opportunity.contacts[0] if @props.opportunity.contacts)
          OpportunityAddressTab
            active: @state.currentTab == 'address'
            opportunity: @props.opportunity
            categories: @props.categories
          OpportunityPublishTab
            active: @state.currentTab == 'publish'

window.OpportunityForm = React.createFactory(OpportunityForm)