DOM = React.DOM

OpportunityHelp = React.createClass
  displayName: 'OpportunityHelp'
  mixins: [PubSubHelper]


  getInitialState: ->
    {
      currentHint: 'about'
    }


  componentDidMount: ->
    @subscribe actions.OPPORTUNITY_EDITOR_TAB_CHANGED, (event_name, tab) =>
      @setState(currentHint: tab)


  render: ->
    DOM.div
      className: 'tab-content'
      OpportunityAboutHint
        active: @state.currentHint == 'about'
      OpportunityDescriptionHint
        active: @state.currentHint == 'description'
      OpportunityContactHint
        active: @state.currentHint == 'contact'
      OpportunityAddressHint
        active: @state.currentHint == 'address'
      OpportunityPublishHint
        active: @state.currentHint == 'publish'
        opportunity: @props.opportunity
        categories: @props.categories

window.OpportunityHelp = React.createFactory(OpportunityHelp)