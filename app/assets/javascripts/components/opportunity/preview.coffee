DOM = React.DOM

OpportunityPreview = React.createClass
  displayName: 'OpportunityPreview'
  mixins: [PubSubHelper, ImageDrawer, GoogleMapsHelper]


  getInitialState: ->
    opportunity = @props.opportunity
    unless opportunity instanceof JsonApiDataStoreModel
      opportunity = (new JsonApiDataStore()).sync(opportunity)

    {
      opportunity: opportunity
    }


  componentDidMount: ->
    @map = @initMap($('#preview__map')[0], @getCords(), @getCategoryClass(), false)

    @drawImage(@state.opportunity.image, $("#preview__image")[0]) if @state.opportunity.image

    @subscribe actions.OPPORTUNITY_EDITOR_TAB_CHANGED, (event_name, tab) =>
      if tab == 'preview'
        # Wait til its rendered correctly
        setTimeout(=>
          @refreshMap(@map)
        , 1)


  componentDidUpdate: ->
    @drawImage(@state.opportunity.image, $("#preview__image")[0]) if @state.opportunity.image

    @redrawMap(@map, @getCords(), @getCategoryClass())


  getCords: ->
    if @state.opportunity.latitude && @state.opportunity.longitude
      {
        lat: @state.opportunity.latitude
        lng: @state.opportunity.longitude
      }


  getCategoryClass: ->
    category = _.invert(@props.categories)[@state.opportunity.category]
    category.split('/')[0].toLowerCase()


  getDate: ->
    start = moment(@state.opportunity.start_date).format('MMM DD')
    end = moment(@state.opportunity.end_date).format('MMM DD')

    [start, end].join(' - ')


  getLocation: ->
    [@state.opportunity.city, @state.opportunity.state].join(', ')


  getContactName: (contact) ->
    contact = @state.opportunity.contacts[0]

    [contact.first_name, contact.last_name].join(' ')


  urlStrip: (url) ->
    url.replace(/(http|https):\/\//, '')


  render: ->
    DOM.div
      className: ''
      DOM.p
        className: 'hk__grotesk-medium table__title'
        'Preview'
      DOM.div
        className: "preview #{@getCategoryClass()}"
        DOM.div
          className: 'preview__header text-center'
          DOM.div
            className: 'preview__image-container'
            if @state.opportunity.image
              DOM.div
                className: ''
                DOM.canvas
                  id: 'preview__image'
                  className: 'canvas__preview'
                  width: '68'
                  height: '68'
                DOM.span
                  className: "preview__category-icon icon icon__#{@getCategoryClass()}--sm"
            else
              DOM.span
                className: "preview__category-plug icon icon__#{@getCategoryClass()}"

          DOM.h2
            className: 'preview__title'
            @state.opportunity.title
          DOM.a
            className: 'preview__header-link'
            href: @state.opportunity.organization.website_url
            @urlStrip(@state.opportunity.organization.website_url)
        DOM.div
          className: 'preview__content'
          DOM.div
            className: 'row preview__details'
            DOM.div
              className: 'col-xs-5'
              @getDate()
            DOM.div
              className: 'col-xs-7 text-right'
              DOM.span
                className: 'preview__location text-uppercase'
                @getLocation()
          DOM.h4
            className: 'preview__content-header hk__grotesk-bold'
            'About'
          DOM.div
            className: 'preview__description text__medium'
            'v-html': 'textarea | marked'
            MarkdownPreview(value: @state.opportunity.description)
          if @state.opportunity.contacts.length
            DOM.div
              className: 'preview__contact-container text__medium'
              DOM.h4
                className: 'preview__content-header hk__grotesk-bold'
                'Contact Person'
              DOM.div
                className: 'row'
                DOM.div
                  className: 'col-xs-3'
                  'Name:'
                DOM.div
                  className: 'col-xs-9'
                  _.truncate(@getContactName(), 40)
              DOM.div
                className: 'row'
                DOM.div
                  className: 'col-xs-3'
                  'Phone:'
                DOM.div
                  className: 'col-xs-9'
                  DOM.a
                    href: "tel:#{@state.opportunity.contacts[0].phone}"
                    @state.opportunity.contacts[0].phone
              DOM.div
                className: 'row'
                DOM.div
                  className: 'col-xs-3'
                  'Email:'
                DOM.div
                  className: 'col-xs-9'
                  DOM.a
                    href: "mailto:#{@state.opportunity.contacts[0].email}"
                    _.truncate(@state.opportunity.contacts[0].email, 40)
          DOM.div
            className: 'preview__map-container'
            DOM.div
              id: 'preview__map'
              className: 'map'
              style:
                height: '260px'
            DOM.div
              className: 'preview__map-title'
              @state.opportunity.address


window.OpportunityPreview = React.createFactory(OpportunityPreview)