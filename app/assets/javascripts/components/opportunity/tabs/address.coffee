DOM = React.DOM

OpportunityAddressTab = React.createClass
  displayName: 'OpportunityAddressTab'
  mixins: [PubSubHelper, GoogleMapsHelper, ValidationHelper]


  getInitialState: ->
    {
      opportunity: @props.opportunity
      valid: false
      errors: {}
      validators:
        address:
          field: 'address'
          selector: '#opportunity__address'
          validations:
            presence: true
    }


  componentDidMount: ->
    searchBox = $('#opportunity__address')[0]

    center = null

    if @state.opportunity.latitude
      center =
        lat: @state.opportunity.latitude
        lng: @state.opportunity.longitude
    
    @initAutocomplete(searchBox, @onSearch)
    @map = @initMap($('#opportunity__map-1')[0], center, @categoryIconName(), true)

    @subscribe actions.OPPORTUNITY_EDITOR_OPPORTUNITY_CHANGED, (event_name, opportunity) =>
      @setState(opportunity: opportunity)

    @subscribe actions.OPPORTUNITY_EDITOR_TAB_CHANGED, (event_name, tab) =>
      if tab == 'address'
        # Wait til its rendered correctly
        setTimeout(=>
          @refreshMap(@map)
        , 1)

    @validate()

  onAddressChange: (event) ->
    value = event.target.value

    opportunity = @state.opportunity
    opportunity.address = value

    @validateField(@state.validators.address, true)

    @setState(opportunity: opportunity)

    @validate()


  validate: (showMessages) ->
    showMessages ||= false

    valid = @validateField(@state.validators.address)

    @publish(actions.OPPORTUNITY_EDITOR_TAB_VALID, { tab: 'address', valid: valid })

    @setState(valid: valid)

    valid


  onSearch: ->
    place = @autocomplete.getPlace()
    location = place.geometry.location
    address = place.formatted_address
    cords =
      lat: location.lat()
      lng: location.lng()

    @redrawMap(@map, cords, @categoryIconName())

    possibleCities = _.filter(place.address_components, (component) -> component.types[0] == 'locality' )
    possibleStates = _.filter(place.address_components, (component) -> component.types[0] == 'administrative_area_level_1' )

    opportunity = @state.opportunity
    opportunity.address = address
    opportunity.latitude = cords.lat
    opportunity.longitude = cords.lng
    opportunity.city = possibleCities[0].short_name if possibleCities.length
    opportunity.state = possibleStates[0].short_name if possibleStates.length

    @setState(opportunity: opportunity)


  onNextClick: (event) ->
    if @validate()
      @publish(actions.OPPORTUNITY_EDITOR_TAB_CHANGED, 'publish')
      @publish(actions.OPPORTUNITY_EDITOR_OPPORTUNITY_CHANGED, @state.opportunity)

  categoryIconName: ->
    category = _.invert(@props.categories)[@state.opportunity.category]
    category.split('/')[0].toLowerCase()


  render: ->
    DOM.div
      id: 'opportunity__tab-address'
      className: "tab-pane tab-pane-white tab-pane--form #{'active' if @props.active}"
      DOM.div
        className: 'tab-pane__content'
        DOM.div
          className: classNames(
            'form-group': true
            'has-error': @hasError('address')
            'has-success': @hasSuccess('address')
          )
          DOM.label
            htmlFor: 'opportunity__address'
            className: 'control-label required'
            'Address'
            if @hasError('address')
              DOM.span
                className: 'form__error pull-right'
                @state.errors.address
          DOM.input
            id: 'opportunity__address'
            className: 'form-control'
            name: 'opportunity[address]'
            onChange: @onAddressChange
            placeholder: '86 Bins Heights Suite 359'
            value: @state.opportunity.address
          DOM.input
            name: 'opportunity[latitude]'
            type: 'hidden'
            value: @state.opportunity.latitude
          DOM.input
            name: 'opportunity[longitude]'
            type: 'hidden'
            value: @state.opportunity.longitude
          DOM.input
            name: 'opportunity[city]'
            type: 'hidden'
            value: @state.opportunity.city
          DOM.input
            name: 'opportunity[state]'
            type: 'hidden'
            value: @state.opportunity.state
        DOM.div
          id: 'opportunity__map-1'
          className: 'map'
          style:
            height: '260px'
      DOM.div
        className: 'tab-pane__actions text-right'
        DOM.div
          className: 'form-group'
          DOM.button
            type: 'button'
            className: 'btn btn-primary btn-lg text-uppercase'
            onClick: @onNextClick
            disabled: !@state.valid
            'Next'

window.OpportunityAddressTab = React.createFactory(OpportunityAddressTab)
