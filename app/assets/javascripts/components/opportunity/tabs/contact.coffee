DOM = React.DOM

OpportunityContactTab = React.createClass
  displayName: 'OpportunityContactTab'
  mixins: [PubSubHelper, ImageDrawer, ValidationHelper]


  getInitialState: ->
    {
      opportunity: @props.opportunity
      valid: false
      errors: {}
      contact: @props.contact || {}
      validators:
        first_name:
          field: 'first_name'
          selector: '#contact__first-name'
          validations:
            presence: true
        last_name:
          field: 'last_name'
          selector: '#contact__last-name'
          validations:
            presence: true
        phone:
          field: 'phone'
          selector: '#contact__phone'
          validations:
            presence: true
        email:
          field: 'email'
          selector: '#contact__email'
          validations:
            presence: true
            email: true
    }

  componentDidMount: ->
    @drawImage(@state.contact.image, $("#contact__image")[0])
    @validate()


  updateOpportunityContact: (contact) ->
    opportunity = @state.opportunity
    opportunity.contacts = [contact]

    @setState
      contact: contact
      opportunity: opportunity


  onContactChange: (event) ->
    id = parseInt(event.target.value)

    contact = if id then (_.filter(@props.contacts, (c) -> return parseInt(c.id) == id )[0]) else {}

    @updateOpportunityContact(contact)

    @drawImage(contact.image, $("#contact__image")[0])

    # wait for render and then revalidate
    setTimeout(=>
      @setState(valid: @validate(true))
    , 1)


  onImageChange: (event) ->
    $input  = $(event.target)
    file    = $input[0].files[0]

    unless file.type.match(/image.*/)
      $input.val('')
      return

    canvas = $("##{$input.data('canvas')}")[0]

    contact = @state.contact
    contact.image = file

    @updateOpportunityContact(contact)

    @drawImage(file, canvas)


  onFirstNameChange: (event) ->
    value = event.target.value

    contact = @state.contact
    contact.first_name = value

    @validateField(@state.validators.first_name, true)

    @updateOpportunityContact(contact)

    @validate()


  onLastNameChange: (event) ->
    value = event.target.value

    contact = @state.contact
    contact.last_name = value

    @validateField(@state.validators.last_name, true)

    @updateOpportunityContact(contact)

    @validate()


  onPhoneChange: (event) ->
    value = event.target.value

    contact = @state.contact
    contact.phone = value

    @validateField(@state.validators.phone, true)

    @updateOpportunityContact(contact)

    @validate()


  onEmailChange: (event) ->
    value = event.target.value

    contact = @state.contact
    contact.email = value

    @validateField(@state.validators.email, true)

    @updateOpportunityContact(contact)

    @validate()


  validate: (showMessages) ->
    showMessages ||= false

    valid = @validateField(@state.validators.first_name, showMessages)
    valid = @validateField(@state.validators.last_name, showMessages) && valid
    valid = @validateField(@state.validators.phone, showMessages) && valid
    valid = @validateField(@state.validators.email, showMessages) && valid

    @publish(actions.OPPORTUNITY_EDITOR_TAB_VALID, { tab: 'contact', valid: valid })

    @setState(valid: valid)

    valid

  onNextClick: (event) ->
    if @validate()
      @publish(actions.OPPORTUNITY_EDITOR_TAB_CHANGED, 'address')
      @publish(actions.OPPORTUNITY_EDITOR_OPPORTUNITY_CHANGED, @state.opportunity)


  render: ->
    DOM.div
      id: 'opportunity__tab-contact'
      className: "tab-pane tab-pane-white tab-pane--form #{'active' if @props.active}"
      DOM.div
        className: 'tab-pane__content'
        DOM.div
          className: classNames(
            'form-group': true
            'has-error': @hasError('contact_ids')
            'has-success': @hasSuccess('contact_ids')
          )
          DOM.label
            htmlFor: 'opportunity__contact'
            className: 'control-label required'
            'Contact Person'
            if @state.errors.contact_ids
              DOM.span
                className: 'form__error pull-right'
                @state.errors.contact_ids
          DOM.select
            id: 'opportunity__contact'
            className: 'form-control'
            name: 'opportunity[contact_ids][]'
            value: @state.contact.id
            onChange: @onContactChange
            DOM.option
              value: ''
              'Add new contact person'
            for contact, n in @props.contacts
              ((i) =>
                DOM.option
                  key: "contact-#{i}"
                  value: contact.id
                  [contact.first_name, contact.last_name].join(' ')
              )(n)
        DOM.div
          className: 'form-group text-center'
          DOM.label
            className: 'control-label canvas__preview-label'
            DOM.canvas
              id: 'contact__image'
              className: 'canvas__preview'
              width: '68'
              height: '68'
            DOM.input
              className: 'hidden'
              name: 'contact[image]'
              type: 'file'
              disabled: @state.contact.id
              'data-canvas': 'contact__image'
              onChange: @onImageChange
            '+ Add photo'
        DOM.div
          className: classNames(
            'form-group': true
            'has-error': @hasError('first_name')
            'has-success': @hasSuccess('first_name')
          )
          DOM.label
            htmlFor: 'contact__first-name'
            className: 'control-label required'
            'First Name'
            if @hasError('first_name')
              DOM.span
                className: 'form__error pull-right'
                @state.errors.first_name
          DOM.input
            id: 'contact__first-name'
            className: 'form-control'
            name: 'contact[first_name]'
            disabled: @state.contact.id
            onChange: @onFirstNameChange
            placeholder: 'Add first name of the contact person'
            value: @state.contact.first_name
        DOM.div
          className: classNames(
            'form-group': true
            'has-error': @hasError('last_name')
            'has-success': @hasSuccess('last_name')
          )
          DOM.label
            htmlFor: 'contact__last-name'
            className: 'control-label required'
            'Last Name'
            if @hasError('last_name')
              DOM.span
                className: 'form__error pull-right'
                @state.errors.last_name
          DOM.input
            id: 'contact__last-name'
            className: 'form-control'
            name: 'contact[last_name]'
            disabled: @state.contact.id
            onChange: @onLastNameChange
            placeholder: 'Add last name of the contact person'
            value: @state.contact.last_name
        DOM.div
          className: classNames(
            'form-group': true
            'has-error': @hasError('phone')
            'has-success': @hasSuccess('phone')
          )
          DOM.label
            htmlFor: 'contact__phone'
            className: 'control-label required'
            'Phone number'
            if @hasError('phone')
              DOM.span
                className: 'form__error pull-right'
                @state.errors.phone
          DOM.input
            id: 'contact__phone'
            className: 'form-control'
            name: 'contact[phone]'
            disabled: @state.contact.id
            onChange: @onPhoneChange
            placeholder: 'Add a phone number of the contact person'
            value: @state.contact.phone
        DOM.div
          className: classNames(
            'form-group': true
            'has-error': @hasError('email')
            'has-success': @hasSuccess('email')
          )
          DOM.label
            htmlFor: 'contact__email'
            className: 'control-label required'
            'E-mail address'
            if @hasError('email')
              DOM.span
                className: 'form__error pull-right'
                @state.errors.email
          DOM.input
            id: 'contact__email'
            className: 'form-control'
            name: 'contact[email]'
            disabled: @state.contact.id
            onChange: @onEmailChange
            placeholder: 'Add an email of the contact person'
            value: @state.contact.email
      DOM.div
        className: 'tab-pane__actions text-right'
        DOM.div
          className: 'form-group'
          DOM.button
            type: 'button'
            className: 'btn btn-primary btn-lg text-uppercase'
            onClick: @onNextClick
            disabled: !@state.valid
            'Next'

window.OpportunityContactTab = React.createFactory(OpportunityContactTab)
