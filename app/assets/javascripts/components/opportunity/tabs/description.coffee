DOM = React.DOM

OpportunityDescriptionTab = React.createClass
  displayName: 'OpportunityDescriptionTab'
  mixins: [PubSubHelper, ValidationHelper]


  getInitialState: ->
    {
      opportunity: @props.opportunity
      valid: false
      errors: {}
      validators:
        title:
          field: 'title'
          selector: '#opportunity__title'
          validations:
            presence: true
            length:
              maximum: 50
        summary:
          field: 'summary'
          selector: '#opportunity__summary'
          validations:
            presence: true
            length:
              maximum: 200
        description:
          field: 'description'
          selector: '#opportunity__description'
          validations:
            presence: true
            length:
              maximum: 1000
    }


  componentDidMount: ->
    autosize($('#opportunity__title, #opportunity__summary, #opportunity__description'))
    @validate()


  componentDidUpdate: ->
    autosize.update($('#opportunity__title, #opportunity__summary, #opportunity__description'))

    
  onTitleChange: (event) ->
    value = event.target.value

    opportunity = @state.opportunity
    opportunity.title = value

    @setState(opportunity: opportunity, -> @validateField(@state.validators.title, true))

    @validate()


  onSummaryChange: (event) ->
    value = event.target.value

    opportunity = @state.opportunity
    opportunity.summary = value

    @setState(opportunity: opportunity, -> @validateField(@state.validators.summary, true))

    @validate()


  onDescriptionChange: (event) ->
    value = event.target.value

    opportunity = @state.opportunity
    opportunity.description = value

    @setState(opportunity: opportunity, -> @validateField(@state.validators.description, true))

    @validate()


  validate: (showMessages) ->
    showMessages ||= false

    valid = @validateField(@state.validators.title, showMessages)
    valid = @validateField(@state.validators.summary, showMessages) && valid
    valid = @validateField(@state.validators.description, showMessages) && valid

    @publish(actions.OPPORTUNITY_EDITOR_TAB_VALID, { tab: 'description', valid: valid })

    @setState(valid: valid)
    
    valid

  onNextClick: (event) ->
    if @validate()
      @publish(actions.OPPORTUNITY_EDITOR_TAB_CHANGED, 'contact')
      @publish(actions.OPPORTUNITY_EDITOR_OPPORTUNITY_CHANGED, @state.opportunity)


  render: ->
    DOM.div
      id: 'opportunity__tab-description'
      className: "tab-pane tab-pane-white tab-pane--form #{'active' if @props.active}"
      DOM.div
        className: 'tab-pane__content'
        DOM.div
          className: classNames(
            'form-group':   true
            'has-error':    @hasError('title')
            'has-success':  @hasSuccess('title')
          )
          DOM.label
            htmlFor: 'opportunity__title'
            className: 'control-label required'
            'Title'
            if @hasError('title')
              DOM.span
                className: 'form__error pull-right'
                @state.errors.title
          DOM.textarea
            id: 'opportunity__title'
            className: 'form-control form__control-huge'
            name: 'opportunity[title]'
            onChange: @onTitleChange
            rows: 1
            placeholder: 'Add title of the opportunity'
            value: @state.opportunity.title
        DOM.div
          className: classNames(
            'form-group':   true
            'has-error':    @hasError('summary')
            'has-success':  @hasSuccess('summary')
          )
          DOM.label
            htmlFor: 'opportunity__summary'
            className: 'control-label required'
            'Summary'
            if @hasError('summary')
              DOM.span
                className: 'form__error pull-right'
                @state.errors.summary
          DOM.textarea
            id: 'opportunity__summary'
            className: 'form-control'
            name: 'opportunity[summary]'
            onChange: @onSummaryChange
            rows: 2
            placeholder: 'Describe the opportunity impact on the community and the constituents served.'
            value: @state.opportunity.summary
        DOM.div
          className: classNames(
            'form-group':   true
            'has-error':    @hasError('description')
            'has-success':  @hasSuccess('description')
          )
          DOM.label
            htmlFor: 'opportunity__description'
            className: 'control-label required'
            'Description'
            if @hasError('description')
              DOM.span
                className: 'form__error pull-right'
                @state.errors.description
          DOM.textarea
            id: 'opportunity__description'
            className: 'form-control'
            name: 'opportunity[description]'
            onChange: @onDescriptionChange
            rows: 2
            placeholder: 'Describe the opportunity impact on the community and the constituents served.'
            value: @state.opportunity.description
      DOM.div
        className: 'tab-pane__actions text-right'
        DOM.div
          className: 'form-group'
          DOM.button
            type: 'button'
            className: 'btn btn-primary btn-lg text-uppercase'
            onClick: @onNextClick
            disabled: !@state.valid
            'Next'

window.OpportunityDescriptionTab = React.createFactory(OpportunityDescriptionTab)
