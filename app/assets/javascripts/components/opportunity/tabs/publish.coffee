DOM = React.DOM

OpportunityPublishTab = React.createClass
  displayName: 'OpportunityPublishTab'
  mixins: [PubSubHelper]

  getInitialState: ->
    {
      status: 1 # after submit set opportunity as 'Published' unless clicked on 'save' button
    }


  componentDidMount: ->
    @subscribe actions.OPPORTUNITY_EDITOR_SAVE_CLICKED, (event_name) =>
      $('input[name="opportunity[status]"]').val(0) # set opportunity as 'Not published'
      $('#opportunity-form').submit()


  render: ->
    DOM.div
      id: 'opportunity__tab-publish'
      className: "tab-pane tab-pane-white tab-pane--form #{'active' if @props.active}"
      DOM.div
        className: 'text-center'
        DOM.h3
          className: ''
          'Congratulations!'
        DOM.p
          className: ''
          'The Opportunity Description should be a brief and straightforward explanation of what the volunteer opportunity entails. Make the description enticing by describing the opportunity impact on the community and the constituents served.'
      DOM.input
        type: 'hidden'
        name: 'opportunity[status]'
        value: @state.status
      DOM.div
        className: 'text-center tab-pane__actions'
        DOM.div
          className: 'form-group'
          DOM.button
            type: 'submit'
            className: 'btn btn-primary btn-lg text-uppercase'
            'Publish'

window.OpportunityPublishTab = React.createFactory(OpportunityPublishTab)
