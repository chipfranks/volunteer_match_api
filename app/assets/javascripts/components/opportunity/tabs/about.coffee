DOM = React.DOM

OpportunityAboutTab = React.createClass
  displayName: 'OpportunityAboutTab'
  mixins: [PubSubHelper, ImageDrawer, UiHelper, ValidationHelper]


  getInitialState: ->
    {
      opportunity: @props.opportunity
      valid: false
      errors: {}
      datepickerStart: moment(@props.opportunity.start_date || undefined).format("YYYY-MM-DD")
      datepickerEnd: moment(@props.opportunity.end_date || undefined).format("YYYY-MM-DD")
      validators:
        category:
          field: 'category'
          selector: '#opportunity__category'
          validations:
            presence: true
        start_date:
          field: 'start_date'
          selector: '#opportunity__date-start'
          validations:
            presence: true
            datetime: true
        end_date:
          field: 'end_date'
          selector: '#opportunity__date-end'
          validations:
            presence: true
            datetime: true
    }

  datepickerFocus: (event) ->
    $('#opportunity__date-start, #opportunity__date-end').removeClass('focus')
    $(event.target).addClass('focus')

    @state.datepicker.show()


  componentDidMount: ->
    @createCategorySelect(@onCategoryChange)
    @drawImage(@state.opportunity.image, $("#opportunity__image")[0])

    daterangepicker = $('#daterangepicker').daterangepicker(
      autoApply:  true
      startDate:  @state.datepickerStart
      endDate:    @state.datepickerEnd
      startField: $('#opportunity__date-start')
      endField:   $('#opportunity__date-end')
      parentEl:   '#opportunity__dates'
      locale:
        format:   'YYYY-MM-DD'
    , (start, end) =>
      opportunity = @state.opportunity
      opportunity.start_date = start.format("YYYY-MM-DD")
      opportunity.end_date = end.format("YYYY-MM-DD")
      @setState
        opportunity: opportunity
        datepickerStart: opportunity.start_date
        datepickerEnd: opportunity.end_date
    ).data('daterangepicker')

    opportunity = @state.opportunity

    unless opportunity.start_date
      opportunity.start_date = moment().format("YYYY-MM-DD")
      opportunity.end_date = moment().format("YYYY-MM-DD")

    @setState
      opportunity: opportunity
      datepicker: daterangepicker

    @validate()

  onImageChange: (event) ->
    $input  = $(event.target)
    file    = $input[0].files[0]

    unless file.type.match(/image.*/)
      $input.val('')
      return

    canvas = $("##{$input.data('canvas')}")[0]

    opportunity = @state.opportunity
    opportunity.image = file

    @setState(opportunity: opportunity)

    @drawImage(file, canvas)


  onCategoryChange: (event) ->
    value = $(event.target).attr('rel')

    opportunity = @state.opportunity
    opportunity.category = value

    @validateField(@state.validators.category, true)

    @setState(opportunity: opportunity)
    @validate()


  onStartDateChange: (event) ->
    value = event.target.value

    opportunity = @state.opportunity
    opportunity.start_date = value

    @validateField(@state.validators.start_date, true)

    @setState(opportunity: opportunity)

    @validate()


  onEndDateChange: (event) ->
    value = event.target.value

    opportunity = @state.opportunity
    opportunity.end_date = value

    @validateField(@state.validators.end_date, true)

    @setState(opportunity: opportunity)

    @validate()


  validate: (showMessages) ->
    showMessages ||= false

    valid = @validateField(@state.validators.category, showMessages)
    valid = @validateField(@state.validators.start_date, showMessages) && valid
    valid = @validateField(@state.validators.end_date, showMessages) && valid

    @publish(actions.OPPORTUNITY_EDITOR_TAB_VALID, { tab: 'about', valid: valid })

    @setState(valid: valid)

    valid

  onNextClick: (event) ->
    if @validate()
      @publish(actions.OPPORTUNITY_EDITOR_TAB_CHANGED, 'description')
      @publish(actions.OPPORTUNITY_EDITOR_OPPORTUNITY_CHANGED, @state.opportunity)


  render: ->
    date_errors = _.compact([].concat(@state.errors.start_date, @state.errors.end_date))

    DOM.div
      id: 'opportunity__tab-about'
      className: "tab-pane tab-pane-white tab-pane--form #{'active' if @props.active}"
      DOM.div
        className: 'tab-pane__content'
        DOM.div
          className: 'form-group text-center'
          DOM.label
            className: 'control-label canvas__preview-label'
            DOM.canvas
              id: 'opportunity__image'
              className: 'canvas__preview'
              width: '68'
              height: '68'
            DOM.input
              className: 'hidden'
              name: 'opportunity[image]'
              type: 'file'
              'data-canvas': 'opportunity__image'
              onChange: @onImageChange
            '+ Add photo'
        DOM.div
          className: classNames(
            'form-group': true
            'has-error': @hasError('category')
            'has-success': @hasSuccess('category')
          )
          DOM.label
            htmlFor: 'opportunity__category'
            className: 'control-label required'
            'Category'
            if @hasError('category')
              DOM.span
                className: 'form__error pull-right'
                @state.errors.category
          DOM.select
            id: 'opportunity__category'
            className: 'form-control select__category'
            name: 'opportunity[category]'
            value: @state.opportunity.category
            onChange: () ->
            for key, val of @props.categories
              DOM.option
                key: "category-#{val}"
                value: val
                key
        DOM.div
          className: classNames(
            'form-group':   true
            'has-error':    !!date_errors.length
            'has-success':  !date_errors.length
          )
          DOM.label
            className: 'control-label required'
            'Date'
            DOM.span
              className: 'form__error pull-right'
              if date_errors.length
                date_errors.join(' / ').toLowerCase()
          DOM.div
            id: 'opportunity__dates'
            className: 'row daterangepicker__wrapper'
            DOM.input
              id: 'opportunity__date-start'
              className: 'form-control text-center'
              type: 'text'
              name: 'opportunity[start_date]'
              onClick: @datepickerFocus
              placeholder: @state.datepickerStart
              value: @state.datepickerStart
              readOnly: true
            DOM.input
              id: 'daterangepicker'
              className: 'hidden'
              type: 'text'
            DOM.span
              className: 'icon icon__arrow--right'
            DOM.input
              id: 'opportunity__date-end'
              className: 'form-control text-center'
              type: 'text'
              name: 'opportunity[end_date]'
              onClick: @datepickerFocus
              placeholder: @state.datepickerEnd
              value: @state.datepickerEnd
              readOnly: true
      DOM.div
        className: 'tab-pane__actions text-right'
        DOM.div
          className: 'form-group'
          DOM.button
            type: 'button'
            className: 'btn btn-primary btn-lg text-uppercase'
            onClick: @onNextClick
            disabled: !@state.valid
            'Next'

window.OpportunityAboutTab = React.createFactory(OpportunityAboutTab)
