DOM = React.DOM

SessionForm = React.createClass
  displayName: 'SessionForm'
  mixins: [PubSubHelper, FacebookHelper]


  getInitialState: ->
    {
      facebookToken: ''
      userName: ''
      userPicture: ''
    }


  componentDidMount: ->
    @loadFbSDK()

    @subscribe actions.FACEBOOK_STATUS_SET, (event_name, response) =>
      if response.status == 'connected'
        @setState(facebookToken: response.authResponse.accessToken)
        @getUser()

    @subscribe actions.FACEBOOK_STATUS_CHANGED, (event_name, response) =>
      if response.status == 'connected'
        @setState(facebookToken: response.authResponse.accessToken)
        @getUser()
      else
        @setState
          facebookToken: ''
          userName: ''
          userPicture: ''


  getUser: ->
    FB.api(
      '/me?fields=first_name,picture',
      (response) =>
        @setState
          userName:     response.first_name
          userPicture:  response.picture.data.url
    )


  onSubmit: (event) ->
    if @state.facebookToken
      # do nothing - the form will be submited
    else
      event.preventDefault()
      @logIn()


  render: ->
    DOM.form
      id: 'session-form'
      encType: 'multipart/form-data'
      action: Routes.new_session_path()
      method: 'POST'
      acceptCharset: 'UTF-8'
      autoComplete: 'off'
      onSubmit: @onSubmit
      DOM.div
        className: 'tab-content'
        DOM.div
          className: "tab-pane tab-pane-white tab-pane--block regular active"
          DOM.input
            type: 'hidden'
            name: 'facebook_token'
            value: @state.facebookToken
          DOM.div
            className: 'form-group text-center'
            DOM.button
              type: 'submit'
              className: classNames(
                'btn btn-fb btn-lg': true
                'has-avatar': @state.userPicture
              )
              if @state.userName
                DOM.span
                  className: ''
                  DOM.span
                    className: 'avatar__preview'
                    style:
                      backgroundImage: "url(#{@state.userPicture})"
                  DOM.span
                    "Continue as "
                  DOM.span
                    className: 'text__gray--light'
                    @state.userName
              else
                DOM.span
                  className: 'text-uppercase'
                  'Sign in with Facebook'


window.SessionForm = React.createFactory(SessionForm)