DOM = React.DOM

SettingsHelp = React.createClass
  displayName: 'SettingsHelp'
  mixins: [PubSubHelper]


  getInitialState: ->
    {
      currentHint: 'about'
    }


  componentDidMount: ->
    @subscribe actions.ORGANIZATION_EDITOR_TAB_CHANGED, (event_name, tab) =>
      @setState(currentHint: tab)


  render: ->
    DOM.div
      className: 'tab-content'
      SettingsAboutHint
        active: @state.currentHint == 'about'

window.SettingsHelp = React.createFactory(SettingsHelp)