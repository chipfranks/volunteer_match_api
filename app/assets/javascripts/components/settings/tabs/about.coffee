DOM = React.DOM

SettingsAboutTab = React.createClass
  displayName: 'SettingsAboutTab'
  mixins: [PubSubHelper, GoogleMapsHelper, ImageDrawer, ValidationHelper]


  getInitialState: ->
    {
      organization: @props.organization
      valid: false
      errors: {}
      validators:
        name:
          field: 'name'
          selector: '#organization__name'
          validations:
            presence: true
        address:
          field: 'address'
          selector: '#organization__address'
          validations:
            presence: true
        website_url:
          field: 'website_url'
          selector: '#organization__website_url'
          validations:
            format:
              pattern: /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/
              message: -> 'is invalid'
    }


  componentDidMount: ->
    searchBox = $('#organization__address')[0]

    @initAutocomplete(searchBox, @onSearch)

    @drawImage(@state.organization.image, $("#organization__image")[0])
    @validate()


  onSearch: ->
    place = @autocomplete.getPlace()
    address = place.formatted_address

    organization = @state.organization
    organization.address = address

    @setState(organization: organization)


  onImageChange: (event) ->
    $input  = $(event.target)
    file    = $input[0].files[0]

    unless file.type.match(/image.*/)
      $input.val('')
      return

    canvas = $("##{$input.data('canvas')}")[0]

    organization = @state.organization
    organization.image = file

    @setState(organization: organization)

    @drawImage(file, canvas)


  onNameChange: (event) ->
    value = $(event.target).val()

    organization = @state.organization
    organization.name = value

    @validateField(@state.validators.name, true)

    @setState(organization: organization)
    @validate()


  onAddressChange: (event) ->
    value = $(event.target).val()

    organization = @state.organization
    organization.address = value

    @validateField(@state.validators.address, true)

    @setState(organization: organization)
    @validate()


  onWebsiteUrlChange: (event) ->
    value = $(event.target).val()

    organization = @state.organization
    organization.website_url = value

    @validateField(@state.validators.website_url, true)

    @setState(organization: organization)
    @validate()


  validate: (showMessages) ->
    showMessages ||= false

    valid = @validateField(@state.validators.name, showMessages)
    valid = @validateField(@state.validators.address, showMessages) && valid
    valid = @validateField(@state.validators.website_url, showMessages) && valid

    @setState(valid: valid)

    valid


  render: ->
    DOM.div
      id: 'settings__tab-about'
      className: "tab-pane tab-pane-white tab-pane--form #{'active' if @props.active}"
      DOM.div
        className: 'tab-pane__content'
        DOM.div
          className: 'form-group text-center'
          DOM.label
            className: 'control-label canvas__preview-label'
            DOM.canvas
              id: 'organization__image'
              className: 'canvas__preview'
              width: '68'
              height: '68'
            DOM.input
              className: 'hidden'
              name: 'organization[image]'
              type: 'file'
              'data-canvas': 'organization__image'
              onChange: @onImageChange
            '+ Add photo'
      DOM.div
        className: classNames(
          'form-group':   true
          'has-error':    @hasError('name')
          'has-success':  @hasSuccess('name')
        )
        DOM.label
          htmlFor: 'organization__name'
          className: 'control-label required'
          'Name'
          if @hasError('name')
            DOM.span
              className: 'form__error pull-right'
              @state.errors.name
        DOM.input
          id: 'organization__name'
          type: 'text'
          className: 'form-control form__control-huge'
          name: 'organization[name]'
          onChange: @onNameChange
          placeholder: 'Add name of the organization'
          value: @state.organization.name
      DOM.div
        className: classNames(
          'form-group': true
          'has-error': @hasError('address')
          'has-success': @hasSuccess('address')
        )
        DOM.label
          htmlFor: 'organization__address'
          className: 'control-label required'
          'Address'
          if @hasError('address')
            DOM.span
              className: 'form__error pull-right'
              @state.errors.address
          DOM.input
            id: 'organization__address'
            className: 'form-control'
            name: 'organization[address]'
            onChange: @onAddressChange
            placeholder: '86 Bins Heights Suite 359'
            value: @state.organization.address
      DOM.div
        className: classNames(
          'form-group':   true
          'has-error':    @hasError('website_url')
          'has-success':  @hasSuccess('website_url')
        )
        DOM.label
          htmlFor: 'organization__website_url'
          className: 'control-label'
          'Website'
          if @hasError('website_url')
            DOM.span
              className: 'form__error pull-right'
              @state.errors.website_url
        DOM.input
          id: 'organization__website_url'
          type: 'text'
          className: 'form-control'
          name: 'organization[website_url]'
          onChange: @onWebsiteUrlChange
          placeholder: 'Add website url of the organization'
          value: @state.organization.website_url
      DOM.div
        className: 'tab-pane__actions text-right'
        DOM.div
          className: 'form-group'
          DOM.button
            type: 'submit'
            className: 'btn btn-primary btn-lg text-uppercase'
            onClick: @onNextClick
            disabled: !@state.valid
            'Save'

window.SettingsAboutTab = React.createFactory(SettingsAboutTab)
