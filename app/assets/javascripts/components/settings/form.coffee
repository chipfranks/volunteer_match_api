DOM = React.DOM

SettingsForm = React.createClass
  displayName: 'SettingsForm'
  mixins: [PubSubHelper]


  getInitialState: ->
    {
      currentTab: 'about'
    }


  componentDidMount: ->
    @subscribe actions.ORGANIZATION_EDITOR_TAB_CHANGED, (event_name, tab) =>
      @setState(currentTab: tab)


  onTabClick: (event) ->
    event.preventDefault()

    target    = $(event.target).attr('href').replace('#settings__tab-', '')
    disabled  = $(event.target).parent().is('.disabled')

    unless disabled
      @publish(actions.ORGANIZATION_EDITOR_TAB_CHANGED, target)


  toggleHint: (event) ->
    $this = $(event.target)
    $this.toggleClass('icon__book--red').toggleClass('icon__dismiss--red')

    @publish(actions.ORGANIZATION_EDITOR_HINT_TOGGLE)


  render: ->
    DOM.form
      id: 'settings-form'
      encType: 'multipart/form-data'
      action: @props.url
      method: 'POST'
      acceptCharset: 'UTF-8'
      autoComplete: 'off'
      DOM.input
        name: '_method'
        type: 'hidden'
        value: @props.http_method
      DOM.a
        href: '#'
        className: 'icon icon__book--red modal-hint__toggle'
        onClick: @toggleHint
      DOM.div
#        DOM.ul
#          className: 'nav nav-tabs nav-justified'
#          DOM.li
#            className: classNames({
#              active: @state.currentTab == 'about'
#            })
#            DOM.a
#              href: '#settings__tab-about'
#              onClick: @onTabClick
#              'About'
        DOM.div
          className: 'tab-content'
          SettingsAboutTab
            active: @state.currentTab == 'about'
            organization: @props.organization


window.SettingsForm = React.createFactory(SettingsForm)