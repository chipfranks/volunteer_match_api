DOM = React.DOM

SettingsEditor = React.createClass
  displayName: 'SettingsEditor'
  mixins: [PubSubHelper]


  getInitialState: ->
    {
      organization: (new JsonApiDataStore()).sync(@props.organization)
    }


  componentDidMount: ->
    @subscribe actions.ORGANIZATION_EDITOR_HINT_TOGGLE, (event_name) =>
      $('.modal-hint').fadeToggle(500)


  render: ->
    DOM.div
      className: 'row'
      DOM.div
        className: 'col-md-6 col-md-offset-0 col-sm-10 col-sm-offset-1'
        SettingsForm
          organization: @state.organization
          url: @props.url
          http_method: @props.http_method
      DOM.div
        className: 'col-md-6 col-md-offset-0 col-sm-10 col-sm-offset-1 modal-hint'
        SettingsHelp
          organization: @state.organization

window.SettingsEditor = React.createFactory(SettingsEditor)