DOM = React.DOM

VideoPlayButton = React.createClass
  displayName: 'VideoPlayButton'
  mixins: [PubSubHelper]


  onClick: (event) ->
    event.preventDefault()

    @publish(actions.VIDEO_PLAY_BUTTON_CLICKED, { videoSrc: @props.videoSrc })


  render: ->
    DOM.a
      className: "btn-play #{@props.className || ''}"
      href: '#'
      onClick: @onClick
      DOM.span
        className: 'icon icon__play'


window.VideoPlayButton = React.createFactory(VideoPlayButton)