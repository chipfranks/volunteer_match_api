DOM = React.DOM

VideoModal = React.createClass
  displayName: 'VideoModal'
  mixins: [PubSubHelper]


  getInitialState: ->
    {
      videoSrc: ''
    }


  componentDidMount: ->
    @subscribe actions.VIDEO_PLAY_BUTTON_CLICKED, (event_name, data) =>
      @setState(videoSrc: data.videoSrc, ->
        $('body').addClass('no-scroll')
        $('.video__modal').fadeIn(500)
      )


  closeModal: (event) ->
    event.preventDefault()
    $('body').removeClass('no-scroll')
    $('.video__modal').fadeOut(500, =>
      @setState(videoSrc: '')
    )


  render: ->
    DOM.div
      className: 'video__modal'
      if @state.videoSrc
        DOM.div
          className: 'video__container'
          DOM.iframe
            className: 'video__iframe'
            src: @state.videoSrc
            webkitAllowFullScreen: true
            mozAllowFullScreen: true
            allowFullScreen: true
            frameBorder: 0
            autoPlay: true
          DOM.a
            className: 'btn-dismiss'
            href: '#'
            onClick: @closeModal
            DOM.span
              className: 'icon icon__dismiss'


window.VideoModal = React.createFactory(VideoModal)