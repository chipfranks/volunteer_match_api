DOM = React.DOM

MarkdownPreview = React.createClass
  displayName: 'MarkdownPreview'

  componentDidMount: ->
    @mountPreview()


  componentDidUpdate: ->
    @._preview.$set('textarea', @props.value || '')


  mountPreview: ->
    @._preview = new Vue
      el: '#markdown__control'
      data:
        textarea: @props.value || ''
      filters:
        marked: marked


  handleDOMReady: (el) ->
    @._el = el

    $(@._el).attr('v-html', 'textarea | marked')


  render: ->
    DOM.div
      id: 'markdown__control'
      ref: @handleDOMReady

window.MarkdownPreview = React.createFactory(MarkdownPreview)