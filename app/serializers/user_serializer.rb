# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  first_name             :string           default("")
#  last_name              :string           default("")
#  image                  :string
#  gender                 :integer          default(0)
#  facebook_uid           :string
#  organization_id        :integer
#  deleted_at             :datetime
#  twitter_uid            :string
#

class UserSerializer < ApplicationSerializer
  attributes :id,
             :first_name,
             :last_name,
             :email,
             :image,
             :gender,
             :facebook_uid,
             :twitter_uid

  has_many :opportunities do
    query_parameters[:deletion_scope] ? object.opportunities.deletion_scope(query_parameters[:deletion_scope]) : object.opportunities
  end

  def image
    object.image_url
  end
end
