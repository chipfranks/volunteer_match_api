# == Schema Information
#
# Table name: contacts
#
#  id              :integer          not null, primary key
#  first_name      :string
#  last_name       :string
#  phone           :string
#  email           :string
#  image           :string
#  organization_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  deleted_at      :datetime
#

class ContactSerializer < ApplicationSerializer
  attributes :id,
             :first_name,
             :last_name,
             :phone,
             :email,
             :image

  belongs_to :organization do
    query_parameters[:deletion_scope] ? Organization.deletion_scope(query_parameters[:deletion_scope]).where(id: object.organization_id).first : object.organization
  end

  def image
    object.image_url
  end
end
