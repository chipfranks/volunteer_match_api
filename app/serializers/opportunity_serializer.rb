# == Schema Information
#
# Table name: opportunities
#
#  id              :integer          not null, primary key
#  title           :text
#  image           :string
#  description     :text
#  summary         :text
#  people_needed   :integer
#  start_date      :datetime
#  end_date        :datetime
#  address         :text
#  latitude        :float
#  longitude       :float
#  organization_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  category        :integer          default(0)
#  city            :string
#  state           :string
#  deleted_at      :datetime
#  status          :integer          default(0)
#

class OpportunitySerializer < ApplicationSerializer
  attributes :id,
             :title,
             :image,
             :description,
             :summary,
             :people_needed,
             :start_date,
             :end_date,
             :address,
             :latitude,
             :longitude,
             :category,
             :city,
             :state,
             :status

  belongs_to :organization do
    query_parameters[:deletion_scope] ? Organization.deletion_scope(query_parameters[:deletion_scope]).where(id: object.organization_id).first : object.organization
  end

  has_many :contacts do
    query_parameters[:deletion_scope] ? object.contacts.deletion_scope(query_parameters[:deletion_scope]) : object.contacts
  end

  has_many :users do
    query_parameters[:deletion_scope] ? object.users.deletion_scope(query_parameters[:deletion_scope]) : object.users
  end

  def image
    object.image_url
  end

  def category
    Opportunity.categories[object.category]
  end

  def status
    Opportunity.statuses[object.status]
  end
end
