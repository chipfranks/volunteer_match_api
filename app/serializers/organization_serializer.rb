# == Schema Information
#
# Table name: organizations
#
#  id          :integer          not null, primary key
#  name        :text
#  image       :string
#  website_url :string
#  address     :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  deleted_at  :datetime
#

class OrganizationSerializer < ApplicationSerializer
  attributes :id,
             :name,
             :image,
             :website_url,
             :address

  has_many :opportunities do
    query_parameters[:deletion_scope] ? object.opportunities.deletion_scope(query_parameters[:deletion_scope]) : object.opportunities
  end

  has_many :contacts do
    query_parameters[:deletion_scope] ? object.contacts.deletion_scope(query_parameters[:deletion_scope]) : object.contacts
  end

  def image
    object.image_url
  end
end
