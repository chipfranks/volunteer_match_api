class ApplicationSerializer < ActiveModel::Serializer
  attributes :deleted_at

  def query_parameters
    instance_options[:serialization_context].try(:query_parameters) || {}
  end
end