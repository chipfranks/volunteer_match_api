# == Schema Information
#
# Table name: user_opportunities
#
#  id             :integer          not null, primary key
#  user_id        :integer
#  opportunity_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  deleted_at     :datetime
#

class UserOpportunitySerializer < ApplicationSerializer
  belongs_to :user do
    query_parameters[:deletion_scope] ? User.deletion_scope(query_parameters[:deletion_scope]).where(id: object.user_id).first : object.user
  end

  belongs_to :opportunity do
    query_parameters[:deletion_scope] ? Opportunity.deletion_scope(query_parameters[:deletion_scope]).where(id: object.opportunity_id).first : object.opportunity
  end
end
