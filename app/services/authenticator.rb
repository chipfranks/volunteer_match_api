require 'twitter'

class Authenticator
  class NoFacebookTokenProvided < StandardError; end
  class NoTwitterTokenProvided < StandardError; end
  class UserAlreadyRegisteredWithFacebook < StandardError; end

  def sign_in(params)
    if params[:email] && params[:password]
      sign_in_with_email(params[:email], params[:password])
    elsif params[:facebook_token].present?
      sign_in_with_facebook(params[:facebook_token])
    elsif params[:twitter_token] && params[:twitter_token_secret]
      sign_in_with_twitter(params[:twitter_token], params[:twitter_token_secret])
    end
  end

  def sign_in_with_email(email, password)
    user = User.find_for_authentication(email: email)

    user if user.present? && user.valid_password?(password)
  end

  def sign_in_with_facebook(facebook_token)
    raise NoFacebookTokenProvided unless facebook_token

    graph   = Koala::Facebook::API.new(facebook_token)
    profile = graph.get_object('me', fields: [:id, :email, :gender, :first_name, :last_name])

    User.find_or_create_by!(facebook_uid: profile['id']) do |user|
      user.first_name       = profile['first_name']
      user.last_name        = profile['last_name']
      user.gender           = profile['gender']
      user.email            = profile['email']
      user.password         = SecureRandom.hex(4)

      if image_url = get_facebook_image_url(profile['id'])
        user.remote_image_url = image_url
      end
    end
  rescue NoFacebookTokenProvided
    nil
  end

  def sign_up_organization_with_facebook(organization_params, facebook_token)
    ActiveRecord::Base.transaction do
      @user = sign_in_with_facebook(facebook_token)

      raise ActiveRecord::Rollback unless @user.is_a? User

      raise UserAlreadyRegisteredWithFacebook if @user.organization

      organization = Organization.create!(organization_params)
      @user.update_attributes(organization_id: organization.id)
      @user.save
    end

    @user
  rescue ActiveRecord::RecordInvalid
    nil
  end

  def sign_in_with_twitter(twitter_token, twitter_token_secret)
    raise NoTwitterTokenProvided unless twitter_token && twitter_token_secret

    client = ::Twitter::REST::Client.new do |config|
      config.consumer_key         = Rails.application.secrets.twitter_consumer_key
      config.consumer_secret      = Rails.application.secrets.twitter_consumer_secret
      config.access_token         = twitter_token
      config.access_token_secret  = twitter_token_secret
    end

    profile = client.verify_credentials

    User.find_or_create_by!(twitter_uid: profile.id) do |user|
      user.first_name, user.last_name = profile['name'].split(' ')
      # Placeholder! We need a special Twitter API Apps feature allowing for fetching user email.
      user.email      = "twitter.user.#{profile.id}@joevolunteer.com"
      user.password   = SecureRandom.hex(4)

      if image_url = profile.profile_image_url
        user.remote_image_url = image_url
      end
    end
  rescue NoTwitterTokenProvided
    nil
  end

  private

  def get_facebook_image_url(profile_id)
    uri       = URI("https://graph.facebook.com/#{profile_id}/picture?type=large&redirect=false")
    response  = Net::HTTP.get(uri)

    data = JSON.parse(response)

    return data['data']['url'] unless data['data']['is_silhouette']
  rescue
    nil
  end
end