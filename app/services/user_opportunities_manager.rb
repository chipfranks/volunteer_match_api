class UserOpportunitiesManager
  def initialize(user, opportunity)
    @user = user
    @opportunity = opportunity
  end

  def join!
    raise User::AlreadyJoined if @user.joined?(@opportunity)

    UserOpportunity.find_or_create_by!(opportunity: @opportunity, user: @user)
  end

  def leave!
    raise User::DidNotJoin unless @user.joined?(@opportunity)

    user_opportunity = UserOpportunity.find_by(opportunity: @opportunity, user: @user)
    user_opportunity.destroy!
  end
end