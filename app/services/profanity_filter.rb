class ProfanityFilter

  def initialize
    dictionary_file = Rails.root.join('config', 'locales', 'dictionary.yml')
    @dictionary = YAML.load_file(dictionary_file)
  end

  def clean(text)
    return text if text.blank?

    text.split(/(\s)/).collect{ |word| clean_word(word) }.join
  end

  def clean_word(word)
    return word unless(word.strip.size > 2)

    if word.index(/[\W]/)
      word    = word.split(/(\W)/).collect{ |sub_word| clean_word(sub_word) }.join
      concat  = word.gsub(/\W/, '')
      word    = concat if banned? concat
    end

    banned?(word) ? @dictionary['dictionary'][word.downcase] : word
  end

  def banned?(word = '')
    @dictionary['dictionary'].include?(word.downcase) if word
  end
end