# JoeVolunteer API Documentation

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Table of Contents

- [General information](#markdown-header-general-information)
- [Dates and times](#markdown-header-dates-and-times)
- [Implementation explanation](#markdown-header-implementation-explanation)
- [Filters](#markdown-header-filters)
- [Orders](#markdown-header-orders)
- [Deletion](#markdown-header-deletion)
- [Pagination](#markdown-header-pagination)
- [Includes](#markdown-header-includes)
- [Error handling](#markdown-header-error-handling)
    - [Fatal errors](#markdown-header-fatal-errors)
- [Authentication / OAuth 2.0](#markdown-header-authentication-oauth-20)
    - [Sign in and sign up](#markdown-header-sign-in-and-sign-up)
    - [Facebook Connect](#markdown-header-facebook-connect)
    - [Refreshing token](#markdown-header-refreshing-token)
    - [Authorization header](#markdown-header-authorization-header)
- [API Entities](#markdown-header-api-entities)
    - [User](#markdown-header-user)
            - [Attributes](#markdown-header-attributes)
            - [Relationships](#markdown-header-relationships)
    - [Organization](#markdown-header-organization)
            - [Attributes](#markdown-header-attributes_1)
            - [Relationships](#markdown-header-relationships_1)
    - [Opportunity](#markdown-header-opportunity)
            - [Attributes](#markdown-header-attributes_2)
            - [Relationships](#markdown-header-relationships_2)
    - [UserOpportunity](#markdown-header-useropportunity)
            - [Relationships](#markdown-header-relationships_3)
    - [Contact](#markdown-header-contact)
            - [Attributes](#markdown-header-attributes_3)
            - [Relationships](#markdown-header-relationships_4)
- [API Endpoints](#markdown-header-api-endpoints)
    - [Authorization](#markdown-header-authorization)
    - [Me](#markdown-header-me)
    - [Organizations](#markdown-header-organizations)
    - [Contacts](#markdown-header-contacts)
    - [Opportunities](#markdown-header-opportunities)
    - [Users](#markdown-header-users)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## General information

Current URL for API are:
 
- staging: `https://staging.joevolunteer.com/api/`
- production: `https://production.joevolunteer.com/api/`

All URLs in this documentation are relative to them.

API is currently partly open. It allows reading some data without authentication.
To see which resources you can reach without login, check the [API Endpoints](#api-endpoints), you will see the `P` (public) sign.

## Dates and times

All dates should be sent in ISO 8601 format to avoid problems - `YYYY-MM-DDThh:mm:ssTZD` where TZD is timezone and is optional (defaults to UTC).

## Implementation explanation

This API tries it's best to implement JSON-API standard but as both the standard and the libraries are still in a state of flux, 
caution is necessary and this documentation should be treated like a source of truth.

Basic JSON structure example:

    {
      "data": {
        "type": "opportunities",
        "id": "1",
        "attributes": {
          "id":"1",
          // other attributes
        },
        "relationships": {
          "owner": {
            // relationship attributes
          }
        }
      }
    }


## Filters

Some endpoints support filters that allow limiting returned collection. They should be passed in GET parameter for collection listing endpoint.

## Orders

Some endpoints support orders that allow ordering returned collection. They should be passed in GET parameter (ex. *?order=name:DESC,id:ASC*) for collection listing endpoint.

## Deletion

*Every* object in the application is deleted using soft-delete method. That means, they are not actually deleted from database, but marked as deleted.

You can fetch records using special filter `deletion_scope`, which gets one of three values:

- without_deleted (default)
- with_deleted
- only_deleted

If you are using `includes` parameter, the same filtering will be applied on relationships too.

Every model has an additional field `deleted_at_` _(boolean)_ containing datetime if record was soft-deleted.

## Pagination

Pagination is being handled in standard JSON-API way, e.g. there's an additional key in returned JSON containing pagination links if applicable.

Example from tests for page 2 out of 3 with 1 item per page:

    {
      "self":"http://test.host/?page%5Bnumber%5D=2\u0026page%5Bsize%5D=1",
      "first":"http://test.host/?page%5Bnumber%5D=1\u0026page%5Bsize%5D=1",
      "prev":"http://test.host/?page%5Bnumber%5D=1\u0026page%5Bsize%5D=1",
      "next":"http://test.host/?page%5Bnumber%5D=3\u0026page%5Bsize%5D=1",
      "last":"http://test.host/?page%5Bnumber%5D=3\u0026page%5Bsize%5D=1"
    }

To simply switch between consecutive pages, you can use 'prev' and 'next' URLs. If you need to go to a certain page, use GET params:

- page[number]="Page number, default: 1"
- page[size]="How many collection items should be visible per page, default: 25"

## Includes

To retrieve additional relationships you have to pass URL parameter `includes` with the request.

You can retrieve multiple relationships in one request by separating them with comma. You can also fetch nested relationships using dot convention.
For example if you were to include User with all his Opportunities including all the Organizations in those opportunities, `includes` would have to be set to `opportunities.organizations`.

You can also use wildcard (`*`) for all relationships and double wildcard (`**`) for all nested relationships.

**Make sure you include ONLY NECESSARY RELATIONS as these kinds of requests put much more strain on the server.**

The included relations will be available in `included` key in JSON:

    {
      "data":{
        "id":"1",
        "type":"opportunities",
        "attributes":{
          ...
        },
        "relationships":{
          "owner":{
            "data":{
              "id":"1",
              "type":"users"
            }
          }
        }
      },
      "included":[
        {
          "id":"1",
          "type":"users",
          "attributes":{
            ...
          }
        }
      ]
    }

## Error handling

The biggest difference between this implementation and standard is error handling. This is caused by the ActiveModel Serializers library being still in development. Error handling is explained below.

If there's a problem with either updating or saving a record, JSON is returned with the following syntax:

    {
        "error": ERROR_LABEL,
        "error_description": "Human-readable information about error that occured",
        "status": 500,
        "errors": [
          "fieldA": "Human-readable information about validation error",
          "fieldB": "etc",
        ]
    }

where ERROR_LABEL = resource name in underscore, preceeded by `invalid_` (ex. invalid_user) or simply 'error' if it's not a resource error.

If there is a non-fatal (as in "not caused by external factors") error, server vill return JSON with the following syntax:

    {
        "error": "general",
        "error_description": "Human-readable information about error that occured",
        "exception_class": "Exception class",
        "status": ERROR_CODE
    }

ERROR_CODE might be 403 for resource you have no rights for, 404 for something that doesn't exist or 500 for general error.

### Fatal errors

There might also be a situation where server returns invalid response due to a major crash (for ex. because Rails server crashed but HTTP one didn't). 
This is unfortunate but can happen so you have to be prepared for that. If something like this happens, 
server throws 500 error and returns HTML content with debugging information. 
Your application will have to handle this accordingly by letting user know that something bad happens, it's not his fault and we apologize.

**Please report every error like this you encounter with exactly what you sent to the server (HTTP request with headers, GET and POST data intact, any additional information you may deem useful). Saying only that "it does not work" or "I tried to update user and it crashed" makes debugging basically impossible.**

## Authentication / OAuth 2.0

### Sign in and sign up

To get an authorization token (`access_token`):

    POST /oauth/token

    - email="YOUR_EMAIL"
    - password="YOUR_PASSWORD"
    - grant_type="password"

Example response from server:

    {
      "access_token":  "cab0d4cbe22445e3f33a208627ae3a1e733af85924d7d91db447fc9c5d2823b4"
      "token_type":    "bearer"
      "expires_in":    3600
      "refresh_token": "18521db721a669c5333380cc9f50ad639ac339adfab099508c326bc37912aaac"
      "scope":         "public"
      "created_at":    1439547112
      "user": "..." _(User)_
    }

### Facebook Connect

To get authorization token using Facebook, you first have to authenticate user on your end. When you recieve OAuth token from Facebook:

    POST /oauth/token

    - grant_type="password"
    - facebook_token="FACEBOOK_TOKEN"

The example response will look identical to email/password authentication.

### Twitter Connect

To get authorization token using Facebook, you first have to authenticate user on your end. When you recieve OAuth token from Facebook:

    POST /oauth/token

    - grant_type="password"
    - twitter_token="TWITTER_TOKEN"
    - twitter_token_secret="TWITTER_TOKEN_SECRET"

The example response will look identical to email/password authentication.

### Refreshing token

To refresh authorization token you have to send POST request to `/oauth/token` with:

    POST /oauth/token

    - grant_type="refresh_token"
    - refresh_token="REFRESH_TOKEN"

### Authorization header

To properly use the authorization token, you have to add HTTP header to every request:

    Authorization: bearer AUTHORIZATION_TOKEN

---

## API Entities

### User

##### Attributes

- email _(string)_
- password _(string)_
- first_name _(string)_
- last_name _(string)_
- image _(multipart/form-data file)_
- gender _(string)_

##### Relationships

- has_many
    - opportunities _(Opportunity)_

### Organization

##### Attributes

- name _(text)_
- image _(multipart/form-data file)_
- website_url _(string)_
- address _(text)_

##### Relationships

- has_many
    - opportunities _(Opportunity)_
    - contacts _(Contact)_

### Opportunity

##### Attributes

- title _(text)_
- image _(multipart/form-data file)_
- description _([markdown](https://daringfireball.net/projects/markdown/syntax))_
- summary _(text)_
- people_needed _(integer)_
- start_date _(datetime)_
- end_date _(datetime)_
- address _(text)_
- latitude _(float)_
- longitude _(float)_
- city _(string)_
- state _(string)_
- category _(integer)_
- status _(integer)_
- contact_ids _(array or comma-separated string)_

Available categories are:

- 0: Poor/Homeless
- 1: Elderly/Sick
- 2: Children/Teens
- 3: Animals
- 4: Community

Available statuses are:

- 0: Not published
- 1: Published
- 2: Closed

##### Relationships

- belongs_to
    - organization _(Organization)_
- has_many
    - contacts _(Contact)_
    - volunteers _(User)_
    
##### Filters

- starts_after _(datetime)_
- starts_before _(datetime)_
- ends_after _(datetime)_
- ends_before _(datetime)_
- updated_after _(datetime)_
- updated_before _(datetime)_
- with_category _(string)_
- with_distance_to_less_than _(string)_ (latitude|longitude|miles)
- with_distance_to_greater_than _(string)_ (latitude|longitude|miles)

### UserOpportunity

##### Relationships

- belongs_to
    - user _(User)_
    - opportunity _(Opportunity)_

### Contact

##### Attributes

- first_name _(string)_
- last_name _(string)_
- phone _(string)_
- email _(string)_
- image _(multipart/form-data file)_

##### Relationships

- belongs_to
    - organization _(Organization)_
- has_many
    - opportunities _(Opportunity)_

## API Endpoints

### Authorization

- POST   /oauth/token
- POST   /oauth/revoke
- GET    /oauth/token/info

### Me

- GET    /me
- POST   /me
- PUT    /me
- GET    /me/opportunities


### Organizations

- GET    /organizations (P)
- POST   /organizations
- GET    /organizations/:id (P)
- PUT    /organizations/:id
- DELETE /organizations/:id

- GET    /organizations/:organization_id/contacts (P)
- POST   /organizations/:organization_id/contacts

- GET    /organizations/:organization_id/opportunities (P)
- POST   /organizations/:organization_id/opportunities


### Contacts

- GET    /contacts (P)
- GET    /contacts/:id (P)
- PUT    /contacts/:id
- DELETE /contacts/:id


### Opportunities

- GET    /opportunities (P)
- GET    /opportunities/:id (P)
- PUT    /opportunities/:id
- DELETE /opportunities/:id

- POST   /opportunities/:id/join
- POST   /opportunities/:id/leave

### Users

- GET    /users/:id/image (P)