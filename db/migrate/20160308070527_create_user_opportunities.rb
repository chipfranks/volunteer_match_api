class CreateUserOpportunities < ActiveRecord::Migration
  def change
    create_table :user_opportunities do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.belongs_to :opportunity, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
