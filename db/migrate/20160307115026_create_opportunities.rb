class CreateOpportunities < ActiveRecord::Migration
  def change
    create_table :opportunities do |t|
      t.text :title
      t.string :image
      t.text :description
      t.text :summary
      t.integer :people_needed
      t.datetime :start_date
      t.datetime :end_date
      t.text :address
      t.float :latitude
      t.float :longitude
      t.belongs_to :organization, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
