class AddStatusToOpportunities < ActiveRecord::Migration
  def change
    add_column :opportunities, :status, :integer, default: 0

    Opportunity.update_all(status: 1)
  end
end
