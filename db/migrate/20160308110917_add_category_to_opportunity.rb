class AddCategoryToOpportunity < ActiveRecord::Migration
  def change
    add_column :opportunities, :category, :integer, default: 0
  end
end
