class CreateOrganizations < ActiveRecord::Migration
  def change
    create_table :organizations do |t|
      t.text :name
      t.string :image
      t.string :website_url
      t.text :address

      t.timestamps null: false
    end
  end
end
