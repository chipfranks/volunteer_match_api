class AddCityAndStateToOpportunity < ActiveRecord::Migration
  def change
    add_column :opportunities, :city, :string
    add_column :opportunities, :state, :string
  end
end
