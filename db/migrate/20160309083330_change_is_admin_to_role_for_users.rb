class ChangeIsAdminToRoleForUsers < ActiveRecord::Migration
  def change
    add_column :users, :role, :integer, default: 0
    remove_column :users, :is_admin, :boolean
  end
end
