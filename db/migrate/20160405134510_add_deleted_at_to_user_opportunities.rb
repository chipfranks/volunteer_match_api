class AddDeletedAtToUserOpportunities < ActiveRecord::Migration
  def change
    add_column :user_opportunities, :deleted_at, :datetime
    add_index :user_opportunities, :deleted_at
  end
end
