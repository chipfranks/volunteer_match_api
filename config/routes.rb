# == Route Map
#
#                                Prefix Verb   URI Pattern                                                 Controller#Action
#                                  root GET    /                                                           home#index
#                            volunteers GET    /volunteers(.:format)                                       home#volunteers
#                    home_organizations GET    /organizations(.:format)                                    home#organizations
#                                 about GET    /about(.:format)                                            home#about
#                        privacy_policy GET    /privacy-policy(.:format)                                   home#privacy_policy
#                          terms_of_use GET    /terms-of-use(.:format)                                     home#terms_of_use
#                     new_admin_session GET    /administration/sign_in(.:format)                           administration/sessions#new
#                         admin_session POST   /administration/sign_in(.:format)                           administration/sessions#create
#                 destroy_admin_session DELETE /administration/sign_out(.:format)                          administration/sessions#destroy
#                        administration GET    /administration(.:format)                                   administration/users#index
#                  administration_users GET    /administration/users(.:format)                             administration/users#index
#                                       POST   /administration/users(.:format)                             administration/users#create
#                   administration_user DELETE /administration/users/:id(.:format)                         administration/users#destroy
#          administration_organizations GET    /administration/organizations(.:format)                     administration/organizations#index
#                                       POST   /administration/organizations(.:format)                     administration/organizations#create
#           administration_organization DELETE /administration/organizations/:id(.:format)                 administration/organizations#destroy
#          administration_opportunities GET    /administration/opportunities(.:format)                     administration/opportunities#index
#                                       POST   /administration/opportunities(.:format)                     administration/opportunities#create
#            administration_opportunity DELETE /administration/opportunities/:id(.:format)                 administration/opportunities#destroy
#                           new_session GET    /login(.:format)                                            sessions#new
#                               session POST   /login(.:format)                                            sessions#create
#                       destroy_session DELETE /logout(.:format)                                           sessions#destroy
#                      new_registration GET    /register(.:format)                                         registrations#new
#                          registration POST   /register(.:format)                                         registrations#create
#                              settings GET    /settings(.:format)                                         settings#edit
#                                       PUT    /settings(.:format)                                         settings#update
#                                       DELETE /settings(.:format)                                         settings#destroy
#                     close_opportunity POST   /opportunities/:id/close(.:format)                          opportunities#close
#                         opportunities GET    /opportunities(.:format)                                    opportunities#index
#                                       POST   /opportunities(.:format)                                    opportunities#create
#                       new_opportunity GET    /opportunities/new(.:format)                                opportunities#new
#                      edit_opportunity GET    /opportunities/:id/edit(.:format)                           opportunities#edit
#                           opportunity GET    /opportunities/:id(.:format)                                opportunities#show
#                                       PATCH  /opportunities/:id(.:format)                                opportunities#update
#                                       PUT    /opportunities/:id(.:format)                                opportunities#update
#                                       DELETE /opportunities/:id(.:format)                                opportunities#destroy
#                           oauth_token POST   /api/oauth/token(.:format)                                  doorkeeper/tokens#create
#                          oauth_revoke POST   /api/oauth/revoke(.:format)                                 doorkeeper/tokens#revoke
#                      oauth_token_info GET    /api/oauth/token/info(.:format)                             doorkeeper/token_info#show
#                                       GET    /api/me(.:format)                                           api/me#index
#                                       POST   /api/me(.:format)                                           api/me#create
#                                       PUT    /api/me(.:format)                                           api/me#update
#                  api_me_opportunities GET    /api/me/opportunities(.:format)                             api/me/opportunities#index
#             api_organization_contacts GET    /api/organizations/:organization_id/contacts(.:format)      api/organizations/contacts#index
#                                       POST   /api/organizations/:organization_id/contacts(.:format)      api/organizations/contacts#create
#        api_organization_opportunities GET    /api/organizations/:organization_id/opportunities(.:format) api/organizations/opportunities#index
#                                       POST   /api/organizations/:organization_id/opportunities(.:format) api/organizations/opportunities#create
#                     api_organizations GET    /api/organizations(.:format)                                api/organizations#index
#                                       POST   /api/organizations(.:format)                                api/organizations#create
#                      api_organization GET    /api/organizations/:id(.:format)                            api/organizations#show
#                                       PATCH  /api/organizations/:id(.:format)                            api/organizations#update
#                                       PUT    /api/organizations/:id(.:format)                            api/organizations#update
#                                       DELETE /api/organizations/:id(.:format)                            api/organizations#destroy
#                          api_contacts GET    /api/contacts(.:format)                                     api/contacts#index
#                           api_contact GET    /api/contacts/:id(.:format)                                 api/contacts#show
#                                       PATCH  /api/contacts/:id(.:format)                                 api/contacts#update
#                                       PUT    /api/contacts/:id(.:format)                                 api/contacts#update
#                                       DELETE /api/contacts/:id(.:format)                                 api/contacts#destroy
#                  join_api_opportunity POST   /api/opportunities/:id/join(.:format)                       api/opportunities#join
#                 leave_api_opportunity POST   /api/opportunities/:id/leave(.:format)                      api/opportunities#leave
#                 close_api_opportunity POST   /api/opportunities/:id/close(.:format)                      api/opportunities#close
#                     api_opportunities GET    /api/opportunities(.:format)                                api/opportunities#index
#                       api_opportunity GET    /api/opportunities/:id(.:format)                            api/opportunities#show
#                                       PATCH  /api/opportunities/:id(.:format)                            api/opportunities#update
#                                       PUT    /api/opportunities/:id(.:format)                            api/opportunities#update
#                                       DELETE /api/opportunities/:id(.:format)                            api/opportunities#destroy
#                        image_api_user GET    /api/users/:id/image(.:format)                              api/users#image
#                 help_phrasing_phrases GET    /phrasing/help(.:format)                                    phrasing_phrases#help
#        import_export_phrasing_phrases GET    /phrasing/import_export(.:format)                           phrasing_phrases#import_export
#                 sync_phrasing_phrases GET    /phrasing/sync(.:format)                                    phrasing_phrases#sync
#             download_phrasing_phrases GET    /phrasing/download(.:format)                                phrasing_phrases#download
#               upload_phrasing_phrases POST   /phrasing/upload(.:format)                                  phrasing_phrases#upload
# remote_update_phrase_phrasing_phrases PUT    /phrasing/remote_update_phrase(.:format)                    phrasing_phrases#remote_update_phrase
#                      phrasing_phrases GET    /phrasing(.:format)                                         phrasing_phrases#index
#                  edit_phrasing_phrase GET    /phrasing/:id/edit(.:format)                                phrasing_phrases#edit
#                       phrasing_phrase PATCH  /phrasing/:id(.:format)                                     phrasing_phrases#update
#                                       PUT    /phrasing/:id(.:format)                                     phrasing_phrases#update
#                                       DELETE /phrasing/:id(.:format)                                     phrasing_phrases#destroy
#               phrasing_phrase_version DELETE /phrasing_phrase_versions/:id(.:format)                     phrasing_phrase_versions#destroy
#

Rails.application.routes.draw do

  root  'home#index'
  get   'volunteers',     to: 'home#volunteers',      as: :volunteers
  get   'organizations',  to: 'home#organizations',   as: :home_organizations
  get   'about',          to: 'home#about',           as: :about
  get   'privacy-policy', to: 'home#privacy_policy',  as: :privacy_policy
  get   'terms-of-use',   to: 'home#terms_of_use',    as: :terms_of_use

  devise_for :admins,
             only: :sessions,
             path: 'administration',
             module: :administration

  namespace :administration do
    get '',   to: 'users#index'

    resources :users,           except: %i(new show edit update)
    resources :organizations,   except: %i(new show edit update)
    resources :opportunities,   except: %i(new show edit update)
  end

  devise_for :users, skip: %i(confirmations sessions registrations passwords omniauth_callbacks)

  as :user do
    get   'login',      to: 'sessions#new',         as: :new_session
    post  'login',      to: 'sessions#create',      as: :session
    match 'logout',     to: 'sessions#destroy',     as: :destroy_session, via: :delete
    get   'register',   to: 'registrations#new',    as: :new_registration
    post  'register',   to: 'registrations#create', as: :registration

    scope :settings do
      get     '', to: 'settings#edit',   as: :settings
      put     '', to: 'settings#update'
      delete  '', to: 'settings#destroy'
    end

    resources :opportunities do
      member do
        post 'close', to: 'opportunities#close'
      end
    end
  end

  scope '/api' do
    use_doorkeeper do
      skip_controllers :authorizations, :applications, :authorized_applications
    end
  end

  namespace :api do
    resources :me, only: :none do
      collection do
        get   '', to: 'me#index'
        post  '', to: 'me#create'
        put   '', to: 'me#update'
      end
    end

    namespace :me do
      resources :opportunities, only: :index
    end

    resources :organizations, except: %i(new edit) do
      resources :contacts,      only: %i(index create), controller: 'organizations/contacts'
      resources :opportunities, only: %i(index create), controller: 'organizations/opportunities'
    end

    resources :contacts,      except: %i(create new edit)

    resources :opportunities, except: %i(create new edit) do
      member do
        post 'join',  to: 'opportunities#join'
        post 'leave', to: 'opportunities#leave'
        post 'close', to: 'opportunities#close'
      end
    end

    resources :users, only: :none do
      member do
        get 'image', to: 'users#image'
      end
    end
  end
end
