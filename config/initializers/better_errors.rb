if defined?(BetterErrors) && ENV["VAGRANT_HOST_PATH"].present?
  BetterErrors.editor = proc { |full_path, line|
    full_path = full_path.sub(Rails.root.to_s, ENV["VAGRANT_HOST_PATH"])
    "txmt://open?url=file://#{full_path}&line=#{line}"
  }

  host = ENV["SSH_CLIENT"] ? ENV["SSH_CLIENT"].match(/\A([^\s]*)/)[1] : nil
  BetterErrors::Middleware.allow_ip! host unless Rails.env.production? || host.blank?
end