require 'test_helper'

class Api::Me::OpportunitiesControllerTest < ActionController::TestCase

  test 'returns user opportunities' do
    authenticate! users(:mobile_only)

    get :index

    assert_json_match(
        {
            data: :opportunities
        }.ignore_extra_keys!, @response.body
    )

    assert_response 200
  end
end
