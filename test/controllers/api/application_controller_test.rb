require 'test_helper'

class ApplicationControllerTestController < Api::ApplicationController
  skip_before_filter :doorkeeper_authorize!, except: %i(authenticate_user_error)

  def standard_error
    raise StandardError
  end

  def not_found
    Organization.find(:non_existent_value)
  end

  def validation_error
    User.create!(email: 'this is not really an email', password: 12345678)
  end

  def authenticate_user_error
    render json: {}
  end
end

class Api::ApplicationControllerTest < ActionController::TestCase

  self.controller_class = ApplicationControllerTestController

  test 'graceful handling of fatal crashes' do
    with_abstract_controller_action :standard_error do
      get :standard_error

      assert_response 500
      assert_json_match(
          {
              error: 'error',
              exception_name: 'StandardError'
          }.ignore_extra_keys!, @response.body
      )
    end
  end

  test 'graceful handling of ActiveRecord not found error' do
    with_abstract_controller_action :not_found do
      get :not_found

      assert_response 404
      assert_json_match(
          {
              error: 'error',
              exception_name: 'ActiveRecord::RecordNotFound'
          }.ignore_extra_keys!, @response.body
      )
    end
  end

  test 'blocking access for unauthenticated user' do
    with_abstract_controller_action :authenticate_user_error do
      get :authenticate_user_error

      assert_response 401
      assert_json_match(
          {
              error: 'invalid_token'
          }.ignore_extra_keys!, @response.body
      )
    end
  end

  test 'graceful handling of ActiveRecord validation errors' do
    with_abstract_controller_action :validation_error do
      get :validation_error

      assert_response 500
      assert_json_match(
          {
              error: 'invalid_user',
              exception_name: 'ActiveRecord::RecordInvalid',
              errors: {
                  email: :email_validation_messages
              }
          }.ignore_extra_keys!, @response.body
      )
    end
  end
end