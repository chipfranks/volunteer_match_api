require 'test_helper'

class Api::MeControllerTest < ActionController::TestCase

  test 'returning currently authenticated user' do
    user = users(:mobile_only)

    authenticate! user

    get :index

    assert_response :success

    assert_json_match(
        {
            data: {
                id:         user.id.to_s,
                type:       'users',
                attributes: {
                    email: user.email
                }.ignore_extra_keys!
            }.ignore_extra_keys!
        }, @response.body
    )
  end

  test 'creating new user' do
    post :create, { email: 'newuser@joevolunteer.com', password: 12345678 }

    assert_response :success

    assert_json_match(
        {
            data: {
                type:       'users',
                attributes: {
                    email: 'newuser@joevolunteer.com'
                }.ignore_extra_keys!
            }.ignore_extra_keys!
        }, @response.body
    )

    assert_not_nil User.find_by(email: 'newuser@joevolunteer.com').try(:id)
  end

  test 'updating currently authenticated user' do
    user = User.find_or_create_by!(email: 'testuser@joevolunteer.com') do |u|
      u.password    = 12345678
      u.first_name  = 'Test'
      u.last_name   = 'User'
    end

    assert_equal 'Test', user.first_name

    authenticate! user

    post :update, { first_name: 'Updated Test' }

    assert_response :success

    assert_json_match(
        {
            data: {
                id:         user.id.to_s,
                type:       'users',
                attributes: {
                    first_name: 'Updated Test'
                }.ignore_extra_keys!
            }.ignore_extra_keys!
        }, @response.body
    )
  end
end
