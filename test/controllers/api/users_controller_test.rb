require 'test_helper'

class Api::UsersControllerTest < ActionController::TestCase

  test 'render user image' do
    user = users(:mobile_only)

    get :image, { id: user.id }

    assert_response :success
  end

  test 'returns not found for user without image' do
    user = users(:additional_user)

    get :image, { id: user.id }

    assert_response 404
    assert_json_match(
        {
            error: 'not found'
        }.ignore_extra_keys!, @response.body
    )
  end
end
