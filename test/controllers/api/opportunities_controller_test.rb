require 'test_helper'

class Api::OpportunitiesControllerTest < ActionController::TestCase

  test 'update opportunity with contacts' do
    authenticate! users(:additional_user)

    opportunity   = opportunities(:standard_organization_current_opportunity)
    contact       = contacts(:standard_organization_contact_first)
    contact_ids   = contact.id

    put :update, { id: opportunity.id, contact_ids: contact_ids.to_s }

    assert_json_match(
        {
            data: {
                relationships: {
                    contacts: {
                        data: [{
                                   id: contact.id.to_s,
                                   type: 'contacts'
                               }]
                    }
                }.ignore_extra_keys!
            }.ignore_extra_keys!
        }, @response.body
    )

    assert_response :success
  end

  test 'join opportunity' do
    user        = users(:mobile_only)
    opportunity = opportunities(:standard_organization_current_opportunity)

    authenticate! user

    assert_difference -> { opportunity.users.count }, 1 do
      post :join, { id: opportunity.id }

      assert_json_match(
          {
              data: {
                  type: 'opportunities'
              }.ignore_extra_keys!
          }, @response.body
      )

      assert_response :success
    end
  end

  test 'throws error if already joined' do
    user        = users(:mobile_only)
    opportunity = opportunities(:standard_organization_future_opportunity)

    authenticate! user

    post :join, { id: opportunity.id }

    assert_response 500
    assert_json_match(
        {
            error: 'error',
            exception_name: 'User::AlreadyJoined'
        }.ignore_extra_keys!, @response.body
    )
  end

  test 'leave opportunity' do
    user        = users(:mobile_only)
    opportunity = opportunities(:standard_organization_future_opportunity)

    authenticate! user

    assert_difference -> { opportunity.users.count }, -1 do
      post :leave, { id: opportunity.id }

      assert_json_match(
          {
              data: {
                  type: 'opportunities'
              }.ignore_extra_keys!
          }, @response.body
      )

      assert_response :success
    end
  end

  test 'throws error if want to leave if not joined' do
    user        = users(:mobile_only)
    opportunity = opportunities(:standard_organization_current_opportunity)

    authenticate! user

    post :leave, { id: opportunity.id }

    assert_response 500
    assert_json_match(
        {
            error: 'error',
            exception_name: 'User::DidNotJoin'
        }.ignore_extra_keys!, @response.body
    )
  end

  test 'close opportunity' do
    user        = users(:mobile_only)
    opportunity = opportunities(:another_organization_future_opportunity)

    authenticate! user

    post :close, { id: opportunity.id }

    assert_json_match(
        {
            data: {
                type: 'opportunities'
            }.ignore_extra_keys!
        }, @response.body
    )

    assert_response :success
  end

  test 'throws error if want to close if already closed' do
    user        = users(:additional_user)
    opportunity = opportunities(:standard_organization_past_opportunity)

    authenticate! user

    post :close, { id: opportunity.id }

    assert_response 500
    assert_json_match(
        {
            error: 'error',
            exception_name: 'Opportunity::AlreadyClosed'
        }.ignore_extra_keys!, @response.body
    )
  end
end
