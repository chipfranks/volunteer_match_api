require 'test_helper'

class CrudControllerTestController < Api::CrudController
  def collection
    Organization.all
  end

  def model_params
    params.permit %i(name address)
  end

  def model_defaults
    { name: 'My Organization' }
  end

  def model_filters
    %i(with_name deletion_scope)
  end

  def model_includes
    %i(opportunities)
  end
end

class Api::CrudControllerTest < ActionController::TestCase
  self.controller_class = CrudControllerTestController

  test 'returning collection of objects' do
    authenticate! users(:mobile_only)

    with_abstract_controller_action :index do
      get :index

      assert_response 200
      matcher = assert_json_match(
          {
              data: :organizations
          }.ignore_extra_keys!, @response.body
      )

      fixtures_count = @loaded_fixtures['organizations'].fixtures.reject { |key, val| val.fixture['deleted_at'] }.size

      assert_equal fixtures_count, matcher.captures[:organizations].size

      organizations_ids = matcher.captures[:organizations].map { |organization| organization['id'] }

      organizations.each do |organization|
        assert_includes organizations_ids, organization.id
      end
    end
  end

  test 'returning filtered collection of objects' do
    authenticate! users(:mobile_only)

    with_abstract_controller_action :index do
      get :index, { with_name: 'Standard' }

      assert_response 200

      matcher = assert_json_match(
          {
              data: :organizations
          }.ignore_extra_keys!, @response.body
      )

      organization = organizations(:standard_organization)
      assert_equal 1, matcher.captures[:organizations].size
      assert_equal organization.id.to_s, matcher.captures[:organizations].first['id']
    end
  end

  test 'returning ordered collection of objects' do
    authenticate! users(:mobile_only)

    with_abstract_controller_action :index do
      get :index, { order: 'name:ASC' }

      assert_response 200

      matcher = assert_json_match(
          {
              data: :organizations
          }.ignore_extra_keys!, @response.body
      )

      organization = organizations(:another_organization)
      assert_equal 3, matcher.captures[:organizations].size
      assert_equal organization.id.to_s, matcher.captures[:organizations].first['id']
    end
  end

  test 'returning deleted objects if needed' do
    authenticate! users(:mobile_only)

    with_abstract_controller_action :index do
      get :index, { deletion_scope: 'with_deleted' }

      assert_response 200

      matcher = assert_json_match(
          {
              data: :organizations
          }.ignore_extra_keys!, @response.body
      )

      assert_equal 4, matcher.captures[:organizations].size
    end
  end

  test 'returning paginated collection of objects' do
    authenticate! users(:mobile_only)

    with_abstract_controller_action :index do
      get :index, { page: { number: 2, size: 1 } }

      assert_response 200

      matcher = assert_json_match(
          {
              links: :links
          }.ignore_extra_keys!, @response.body
      )

      assert matcher.captures[:links].present?
      assert matcher.captures[:links]['self'].present?,  'Link to self is missing'
      assert matcher.captures[:links]['first'].present?, 'Link to first object is missing'
      assert matcher.captures[:links]['prev'].present?,  'Link to previous object is missing'
      assert matcher.captures[:links]['next'].present?,  'Link to next object is missing'
      assert matcher.captures[:links]['last'].present?,  'Link to last object is missing'
    end
  end

  test 'returning collection with includes' do
    authenticate! users(:mobile_only)

    opportunity = opportunities(:standard_organization_future_opportunity)

    with_abstract_controller_action :index do
      get :index, { includes: 'opportunities' }

      assert_response 200

      assert_json_match(
          {
              included: [{
                             id: opportunity.id.to_s,
                             type: 'opportunities'
                         }.ignore_extra_keys!].ignore_extra_values!
          }.ignore_extra_keys!, @response.body
      )
    end
  end

  test 'returning one object' do
    authenticate! users(:mobile_only)

    organization = organizations(:standard_organization)

    with_abstract_controller_action :show do
      get :show, { id: organization.id }

      assert_response 200
      assert_json_match(
          {
              data: {
                  id: organization.id.to_s
              }.ignore_extra_keys!
          }.ignore_extra_keys!, @response.body
      )
    end
  end

  test 'returning one object with includes' do
    authenticate! users(:mobile_only)

    organization = organizations(:standard_organization)
    opportunity  = opportunities(:standard_organization_future_opportunity)

    with_abstract_controller_action :show do
      get :show, { id: organization.id, includes: 'opportunities' }

      assert_response 200

      assert_json_match(
          {
              included: [{
                             id: opportunity.id.to_s,
                             type: 'opportunities'
                         }.ignore_extra_keys!].ignore_extra_values!
          }.ignore_extra_keys!, @response.body
      )
    end
  end

  test 'creating a new object' do
    authenticate! users(:mobile_only)

    existing_organization_ids = Organization.pluck(:id)

    assert_difference 'Organization.count' do
      with_abstract_controller_action :create, :post do
        post :create, { name: 'New Organization', address: 'New Organization Str. 123' }

        assert_response 200

        matcher = assert_json_match(
            {
                data: {
                    id: :new_organization_id,
                    attributes: { name: 'New Organization' }.ignore_extra_keys!
                }.ignore_extra_keys!
            }.ignore_extra_keys!, @response.body
        )

        assert_not_includes existing_organization_ids, matcher.captures[:new_organization_id].to_i
      end
    end
  end

  test 'updating existing object' do
    authenticate! users(:mobile_only)

    organization = organizations(:standard_organization)

    with_abstract_controller_action :update, :post do
      post :update, { id: organization.id, name: 'My Updated Organization' }

      assert_response 200

      assert_json_match(
          {
              data: {
                  id: organization.id.to_s,
                  attributes: { name: 'My Updated Organization' }.ignore_extra_keys!
              }.ignore_extra_keys!
          }.ignore_extra_keys!, @response.body
      )
    end
  end

  test 'destroying existing object' do
    authenticate! users(:mobile_only)

    organization = organizations(:standard_organization)

    assert_difference 'Organization.count', -1 do
      with_abstract_controller_action :destroy, :delete do
        delete :destroy, { id: organization.id }
        assert_response 200
      end
    end
  end
end
