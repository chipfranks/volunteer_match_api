require 'test_helper'

class Api::Organizations::OpportunitiesControllerTest < ActionController::TestCase

  test 'creating opportunity with contacts' do
    authenticate! users(:mobile_only)

    organization = organizations(:standard_organization)
    contact = contacts(:standard_organization_contact_first)
    contact_ids = contact.id

    post :create, { organization_id: organization.id, contact_ids: contact_ids.to_s }

    assert_json_match(
        {
            data: {
              relationships: {
                contacts: {
                  data: [{
                    id: contact.id.to_s,
                    type: 'contacts'
                  }]
                }
              }.ignore_extra_keys!
            }.ignore_extra_keys!
        }, @response.body
    )

    assert_response :success
  end
end
