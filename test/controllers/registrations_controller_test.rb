require 'test_helper'

class RegistrationsControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  test 'redirects if already signed in' do
    user = users(:mobile_only)
    sign_in user

    get :new

    assert_redirected_to opportunities_path
  end

  test 'renders form if not signed in' do
    get :new

    assert_response :success
  end

  test 'creates user with organization' do
    # Allow external requests!
    # TODO: make something more clever
    WebMock.disable!

    test_users = Koala::Facebook::TestUsers.new(
        app_id: Rails.application.secrets.facebook_app_id,
        secret: Rails.application.secrets.facebook_app_secret
    )

    fake_user = test_users.create(true, 'public_profile, email')

    facebook_token = fake_user['access_token']

    assert_difference ['User.count', 'Organization.count'] do
      post :create, facebook_token: facebook_token,
           organization: { name: 'Test Organization Name', address: 'Test Str. 123' }
    end

    assert_redirected_to opportunities_path

    WebMock.enable!
  end

  test 'does not create user with organization if no facebook token' do
    assert_no_difference ['User.count', 'Organization.count'] do
      post :create, organization: { name: 'Test Organization Name', address: 'Test Str. 123' }
    end
  end

  test 'does not create user with organization if organization invalid' do
    # Allow external requests!
    # TODO: make something more clever
    WebMock.disable!

    test_users = Koala::Facebook::TestUsers.new(
        app_id: Rails.application.secrets.facebook_app_id,
        secret: Rails.application.secrets.facebook_app_secret
    )

    fake_user = test_users.create(true, 'public_profile, email')

    facebook_token = fake_user['access_token']

    assert_no_difference ['User.count', 'Organization.count'] do
      post :create, facebook_token: facebook_token,
           organization: { name: 'Test Organization Name' }
    end
  end
end
