require 'test_helper'

class SessionsControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  test 'redirects if already signed in' do
    user = users(:mobile_only)
    sign_in user

    get :new

    assert_redirected_to opportunities_path
  end

  test 'renders form if not signed in' do
    get :new

    assert_response :success
  end

  test 'creates and signs in user from facebook' do
    # Allow external requests!
    # TODO: make something more clever
    WebMock.disable!

    test_users = Koala::Facebook::TestUsers.new(
        app_id: Rails.application.secrets.facebook_app_id,
        secret: Rails.application.secrets.facebook_app_secret
    )

    fake_user = test_users.create(true, 'public_profile, email')

    facebook_token = fake_user['access_token']

    assert_difference 'User.count' do
      post :create, facebook_token: facebook_token
    end

    # newly created user has no organization, so redirect to registration
    assert_redirected_to new_registration_path

    WebMock.enable!
  end

  test 'renders form if error signing in' do
    post :create

    assert_template :new
  end

  test 'signs out user' do
    user = users(:mobile_only)
    sign_in user

    delete :destroy

    assert_redirected_to home_organizations_path
  end
end
