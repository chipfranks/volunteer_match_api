require 'test_helper'

class HomeControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  test 'index is accessible' do
    get :index

    assert_response :success
  end

  test 'volunteers is accessible' do
    get :volunteers

    assert_response :success
  end

  test 'organizations is accessible' do
    get :organizations

    assert_response :success
  end
end
