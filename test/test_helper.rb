require 'simplecov'

SimpleCov.start do
  add_filter '/test/'
  add_filter '/config/'
  add_filter '/lib/'
  add_filter '/vendor/'

  add_group 'Controllers', 'app/controllers'
  add_group 'Models', 'app/models'
  add_group 'Helpers', 'app/helpers'
  add_group 'Mailers', 'app/mailers'
  add_group 'Views', 'app/views'
  add_group 'Services', 'app/services'
  add_group 'Uploaders', 'app/uploaders'
  add_group 'Policies', 'app/policies'
  add_group 'Serializers', 'app/serializers'
end

ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'minitest/mock'
require 'webmock/minitest'
require 'json_expressions/minitest'

require 'minitest/stub_any_instance'

require 'minitest/reporters'
Minitest::Reporters.use!

Rails.application.eager_load! if ENV['COVERAGE']

class CarrierWave::Mount::Mounter
  def store!
    # Not storing uploads in the tests
  end
end

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml
  fixtures :all
  self.test_order = :random
  self.use_instantiated_fixtures = true

  # Add more helper methods to be used by all tests here...

  CarrierWave.root = 'test/fixtures/files'

  def after_teardown
    super
    CarrierWave.clean_cached_files!(0)
  end

  def authenticate!(user)
    access_token = Doorkeeper::AccessToken.find_or_create_by!(resource_owner_id: user.id,
                                                              expires_in:        86400,
                                                              scopes:            'public')
    access_token.update!(refresh_token: access_token.send(:generate_token)) if access_token.refresh_token.nil?
    @request.headers['Authorization'] = "bearer #{access_token.token}"
    access_token
  end

  def with_abstract_controller(controller_name, action_name, method=:get, &block)
    with_routing do |set|
      set.draw do
        send(method, '/' => "#{controller_name}##{action_name}")
      end

      block.call
    end
  end

  def with_abstract_controller_action(action_name, method=:get, &block)
    controller_name = self.class.controller_class.name.sub(/Controller$/, '').underscore
    with_abstract_controller(controller_name, action_name, method, &block)
  end
end

module FixtureHelpers
end

ActiveRecord::FixtureSet.context_class.include FixtureHelpers