# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  first_name             :string           default("")
#  last_name              :string           default("")
#  image                  :string
#  gender                 :integer          default(0)
#  facebook_uid           :string
#  organization_id        :integer
#  deleted_at             :datetime
#  twitter_uid            :string
#

require 'test_helper'

class UserTest < ActiveSupport::TestCase

  test 'has default image' do
    user = User.new({
                        email: 'new.user@joevolunteer.com',
                        password: 'password'
                    })

    user.save!

    assert user.image.present?
  end

  test 'has uploaded image' do
    user = User.new({
                        email: 'new.user@joevolunteer.com',
                        password: 'password',
                        image: File.open('./test/fixtures/files/placeholder.png')
                    })

    user.save!

    assert user.image.present?
  end
end
