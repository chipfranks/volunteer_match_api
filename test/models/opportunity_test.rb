# == Schema Information
#
# Table name: opportunities
#
#  id              :integer          not null, primary key
#  title           :text
#  image           :string
#  description     :text
#  summary         :text
#  people_needed   :integer
#  start_date      :datetime
#  end_date        :datetime
#  address         :text
#  latitude        :float
#  longitude       :float
#  organization_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  category        :integer          default(0)
#  city            :string
#  state           :string
#  deleted_at      :datetime
#  status          :integer          default(0)
#

require 'test_helper'

class OpportunityTest < ActiveSupport::TestCase
  test 'that filters are working correctly' do
    opportunities_starts_before  = Opportunity.filter_with(starts_before: Time.now + 30.minutes)
    opportunities_starts_after   = Opportunity.filter_with(starts_after: Time.now + 30.minutes)
    opportunities_ends_before    = Opportunity.filter_with(ends_before: Time.now + 2.hours + 30.minutes)
    opportunities_ends_after     = Opportunity.filter_with(ends_after: Time.now + 2.hours + 30.minutes)
    opportunities_updated_before = Opportunity.filter_with(updated_before: Time.now - 1.hour + 30.minutes)
    opportunities_updated_after  = Opportunity.filter_with(updated_after: Time.now - 1.hour + 30.minutes)

    opportunities_with_category  = Opportunity.filter_with(with_category: 0)

    opportunities_with_distance_to_less_than    = Opportunity.filter_with(with_distance_to_less_than:     '32.00|105.00|75.0')
    opportunities_with_distance_to_greater_than = Opportunity.filter_with(with_distance_to_greater_than:  '32.00|105.00|75.0')

    opportunities_with_status    = Opportunity.filter_with(with_status: 0)
    opportunities_not_published  = Opportunity.not_published
    opportunities_published      = Opportunity.published
    opportunities_closed         = Opportunity.closed

    assert_equal 2, opportunities_starts_before.count
    assert_equal 2, opportunities_starts_after.count
    assert_equal 3, opportunities_ends_before.count
    assert_equal 1, opportunities_ends_after.count
    assert_equal 3, opportunities_updated_before.count
    assert_equal 1, opportunities_updated_after.count
    assert_equal 3, opportunities_with_category.count
    assert_equal 2, opportunities_with_distance_to_less_than.count
    assert_equal 2, opportunities_with_distance_to_greater_than.count
    assert_equal 1, opportunities_with_status.count
    assert_equal 1, opportunities_not_published.count
    assert_equal 2, opportunities_published.count
    assert_equal 1, opportunities_closed.count
  end
end
