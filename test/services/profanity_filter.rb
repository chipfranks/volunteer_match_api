require 'test_helper'

class ProfanityFilterTest < ActiveSupport::TestCase

  test 'cleans text' do
    profanity_filter = ProfanityFilter.new

    # generated using http://loremfuckingipsum.com/
    text = %q(
      A good fucking composition is the result of a hierarchy consisting of clearly contrasting elements
      set with distinct alignments containing irregular intervals of negative space.
      If you’re not being fucking honest with yourself how could you ever hope to communicate
      something meaningful to someone else? Someday is not a fucking day of the week.
      Your rapidograph pens are fucking dried up, the x-acto blades in your bag are rusty,
      and your mind is dull. Stop clicking your mouse, get messy, go back to the basics
      and make something fucking original. If you fucking give up, you will achieve nothing.
      Fuck. Widows and orphans are terrible fucking tragedies, both in real life and definitely in typography.
      Use your fucking hands. Paul Rand once said, “The public is more familiar with bad fucking
      design than good design. It is, in effect, conditioned to prefer bad design,
      because that is what it lives with. The new becomes threatening, the old reassuring.”
      The graphic designer’s first fucking consideration is always the size and shape of the format,
      whether for the printed page or for digital display.
      Make your work consistent but not fucking predictable.
      This design is fucking brilliant.
    )

    # cleaned using http://www.noswearing.com/
    censored_text = %q(
      A good freaking composition is the result of a hierarchy consisting of clearly contrasting elements
      set with distinct alignments containing irregular intervals of negative space.
      If you’re not being freaking honest with yourself how could you ever hope to communicate
      something meaningful to someone else? Someday is not a freaking day of the week.
      Your rapidograph pens are freaking dried up, the x-acto blades in your bag are rusty,
      and your mind is dull. Stop clicking your mouse, get messy, go back to the basics
      and make something freaking original. If you freaking give up, you will achieve nothing.
      fornicate. Widows and orphans are terrible freaking tragedies, both in real life and definitely in typography.
      Use your freaking hands. Paul Rand once said, “The public is more familiar with bad freaking
      design than good design. It is, in effect, conditioned to prefer bad design,
      because that is what it lives with. The new becomes threatening, the old reassuring.”
      The graphic designer’s first freaking consideration is always the size and shape of the format,
      whether for the printed page or for digital display.
      Make your work consistent but not freaking predictable.
      This design is freaking brilliant.
    )

    assert_equal profanity_filter.clean(text), censored_text
  end
end
