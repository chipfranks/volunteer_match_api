require 'test_helper'

class AuthenticatorTest < ActiveSupport::TestCase

  def setup
    @authenticator = Authenticator.new
  end

  test 'authentication with email and password' do
    user = @authenticator.sign_in(email: 'additional.user@joevolunteer.com', password: 12345678)

    assert_kind_of User, user
  end

  test 'authentication with facebook token' do
    # Allow external requests!
    # TODO: make something more clever
    WebMock.disable!

    test_users = Koala::Facebook::TestUsers.new(
        app_id: Rails.application.secrets.facebook_app_id,
        secret: Rails.application.secrets.facebook_app_secret
    )

    fake_user = test_users.create(true, 'public_profile, email')

    user = @authenticator.sign_in(facebook_token: fake_user['access_token'])

    assert_kind_of User, user

    WebMock.enable!
  end
end
