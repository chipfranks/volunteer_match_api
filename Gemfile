source 'https://rubygems.org'

gem 'rails', '4.2.5'
gem 'pg'

# Front-end
gem 'sass-rails', '~> 5.0'
gem 'bootstrap-sass', '~> 3.3.6'
gem 'bootstrap_form'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.1.0'
gem 'jquery-rails'
gem 'haml-rails'
gem 'react-rails', '~> 1.6.0'
gem 'js-routes'

# API
gem 'active_model_serializers', '0.10.0.rc4'
gem 'doorkeeper'
gem 'carrierwave'
gem 'mini_magick'

# Deployment
gem 'puma'
gem 'mina'
gem 'mina-puma', require: false

# Development
group :development, :test do
  gem 'spring'
end

group :development do
  gem 'annotate'
  gem 'quiet_assets'
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'pry-rescue'
  gem 'pry-stack_explorer'
end

# Test
group :test do
  gem 'minitest-reporters', require: false
  gem 'webmock', require: false
  gem 'json_expressions', require: false
  gem 'simplecov', require: false
  gem 'minitest-stub_any_instance', require: false
  gem 'timecop'
  gem 'm', '~> 1.3.1'
end

# External integrations
gem 'twitter'
gem 'koala', '~> 2.2'
gem 'omniauth-facebook'
gem 'newrelic_rpm'
gem 'rollbar', '~> 2.1.2'
gem 'rmagick', require: false

# Various gems
gem 'devise'
gem 'kaminari'
gem 'pundit'
gem 'faker'
gem 'pry-rails'
gem 'pry-byebug'
gem 'cocaine'
gem 'activerecord-enum-without-methods', '~> 1.0'
gem 'paranoia', '~> 2.0'
gem 'phrasing'